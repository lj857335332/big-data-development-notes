#!/bin/bash

#通过访问check.html页面来检查nginx是否宕机
url="http://127.0.0.1/check.html"

code=`curl -s -o /dev/null -w %{http_code} $url`
#通过判断相应编码是否是200来确定nginx宕机
if [ $code -ne 200 ];then
  sleep 1
  code=`curl -s -o /dev/null -w %{http_code} $url`
  if [ $code -ne 200 ];then
    #确定nginx是宕机，关闭本机的keepalived
    service keepalived stop
  fi
fi
