[TOC]



# 02 Nginx和Keepalived高可用

## 1. 为什么要学习keepalived？

![image-20211216215054816](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112162150465.png)

&emsp;&emsp;Nginx如果存在单点故障，即使后面两个节点都是可用的，也不能提供服务，那就提供多个Nginx负载均衡服务器。但是客户端找哪个Nginx呢？这个时候，就用到**Keepalived**。有了Keepalived，可以让Nginx1成为 Keepalived Master。让Nginx2成为 Keepalived backup。Nginx1对外虚拟一个IP，比如：200。当客户端访问时，可以访问200这个虚拟ip。只有当Nginx1出现故障时，Keepalived可以判断出来，此时，虚拟ip指向Nginx2，Nginx2才会提供服务。**Keepalived可以实现主备切换！！！**

## 2. Keepalived概述

&emsp;&emsp;keepalived 是集群管理中保证集群高可用的服务软件。**Keepalived 的作用是检测服务器的状态**，如果有一台服务器宕机，或工作出现故障，Keepalived 将检测到，并将有故障的服务器从系统中剔除，同时使用其他服务器代替该服务器的工作，当服务器工作正常后 Keepalived 自动将服务器加入到服务器群中，这些工作全部自动完成，不需要人工干涉，需要人工做的只是修复故障的服务器。

**高可用**：

1. 需要心跳机制探测后端 RS 是否提供服务
   - 探测 down，需要从 lvs 中删除该 RS
   - 探测发送从 down 到 up，需要从 lvs 中再次添加 RS

2. Lvs DR，需要主备（HA）

**Keepalived原理**：

VRRP 协议（虚拟路由冗余协议） - Virtual Router Redundancy Protocol

![image-20211216221731267](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112162217637.png)

keepalived 工作在 IP/TCP 协议栈的 IP 层（3），TCP 层（4），及应用层（5），工作原理基于 VRRP 协议。

- 网络层(layer 3)：Keepalived 会定期向服务器群中的服务器发送一个 ICMP 的数据包， （既我们平时用的 ping 程序）,如果发现某台服务的 IP 地址没有激活，Keepalived 便报告 这台服务器失效，并将它从服务器群中剔除。
- 传输层(layer 4)：Keepalived 以 TCP 端口的状态来决定服务器工作正常与否，如 web server 的服务端口一般是 80，如果 Keepalived 检测到 80 端口没有启动，则 Keepalived 将把这台服务器从服务器群中剔除。
- 应用层(layer 5)：只要针对应用上的一些探测方式，如 URL 的 get 请求，或者对 nginx脚本检测等；可以根据用户自定义添加脚本针对特定的服务进行状态检测，当检测结果与用 户设定不一致，则把这台服务器从服务器群中剔除

## 3. VRRP协议与工作原理

![image-20211216222059933](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112162221997.png)

&emsp;&emsp;VRRP(Virtual Router Redundancy Protocol)虚拟路由冗余协议是一种容错的主备模式的协议，当网络设备发生故障时，可以不影响主机之间通信情况下进行**设备主备切换**，并且相对用户时切换过程时透明的。

&emsp;&emsp;一个 VRRP 路由器有唯一的标识：VRID，范围为 0—255｡该路由器对外表现为唯一的虚拟 MAC 地址，地址的格式为 00-00-5E-00-01-[VRID]。

&emsp;&emsp;**同一台路由器可以加入多个备份组，在不同备份组中有不同的优先级，使得该路由器可以在一个备份组中作为主用路由器，在其他的备份组中作为备用路由器**。

提供了两种**安全认证措施**：明文认证和 IP 头认证｡ 

## 4. VRRP选举机制

- 虚拟 IP 拥有者：如果某台路由器的 IP 地址与虚拟路由器的 VIP **地址一致**，那么这台就会被选为**主路由器**。 
- 优先级较高者，如果没有虚拟 IP 拥有者，**优先级数值大**的路由器会被选举出成为master，优先级范围 0~255。 

- 如果优先级一样高，**IP 地址数值大**的路由器会被选举出。 
  - 192.168.58.11       192.168.58.24 MASTER

## 5. 安装前准备

&emsp;&emsp;nginx1 和 nginx2 作为负载均衡服务器，node2 和 node3 还是 RS 服务器，并修改 node2 和 node3 上的 index.jps，去掉样式和图片，去掉动静分离的干扰。即这里的nginx不再作为静态服务器，而是作为负载均衡服务器。

node2:

```
[root@node2 ~]# vim /opt/tomcat-7.0.69/webapps/ROOT/index.jsp 

from 192.168.236.102 <br/>
session=<%=session.getId()%></font>
```

node3:

```
[root@node3 ~]# vim /opt/tomcat-7.0.69/webapps/ROOT/index.jsp 

from 192.168.236.103 <br/>
session=<%=session.getId()%></font>
```

## 6.安装步骤

1. 修改 nginx1 和 nginx2 的/etc/hosts 配置文件，添加：

   ```
   196.168.236.105 nginx1
   196.168.236.106 nginx2
   ```

   ```
   [root@nginx1 ~]# vim /etc/hosts
   
   127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
   ::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
   192.168.236.100 node0
   192.168.236.101 node1
   192.168.236.102 node2
   192.168.236.103 node3
   192.168.236.104 node4
   192.168.236.105 nginx1
   192.168.236.106 nginx2
   ```

   ```
   [root@nginx2 ~]# vim /etc/hosts
   
   127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
   ::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
   192.168.236.100 node0
   192.168.236.101 node1
   192.168.236.102 node2
   192.168.236.103 node3
   192.168.236.104 node4
   192.168.236.105 nginx1
   192.168.236.106 nginx2
   ```

2. 修改 nginx1 上的 nginx.conf 文件 

   ```
   [root@nginx1 ~]# cd /opt/nginx/conf/
   [root@nginx1 conf]# vim nginx.conf
   
   upstream rss {
         server 192.168.236.102:8080;
         server 192.168.236.103:8080;
       }
       server {
           listen 80;
           server_name localhost;
           location / {
             root html;
           }
       }
       server {
           listen       80;
           server_name  192.168.236.200;
           location / {
             proxy_pass http://rss/;
           }
   
       }
   ```

3. 备份 nginx2 的配置文件 nginx.conf,将 nginx1 上的配置文件远程拷贝到 nginx2上

   ```
   [root@nginx1 conf]# scp nginx.conf nginx2:`pwd`
   The authenticity of host 'nginx2 (192.168.236.106)' can't be established.
   RSA key fingerprint is 8e:0e:3f:82:c9:aa:4f:bd:4d:b0:b3:9c:82:a7:b9:86.
   Are you sure you want to continue connecting (yes/no)? 123456
   Please type 'yes' or 'no': yes
   Warning: Permanently added 'nginx2,192.168.236.106' (RSA) to the list of known hosts.
   root@nginx2's password: 
   nginx.conf                                                                  100% 1066     1.0KB/s   00:00    
   ```

4. 在 nginx1 的/home/目录下编写 check_nginx.sh脚本，定期检查nginx是否活着

   ```
   #!/bin/bash
   
   #通过访问check.html来检查nginx是否宕机
   url="http://127.0.0.1/check.html"
   #获取请求后的状态码
   code=`curl -s -o /dev/null -w %{http_code} $url`
   
   #通过判断相应状态码是否是200来确定nginx宕机，是200即为正常
   if [ $code -ne 200 ];then
     #休眠一秒
     sleep 1
     #再发送请求获取状态码
     code=`curl -s -o /dev/null -w %{http_code} $url`
     #再判断相应状态码是否是200
     if [ $code -ne 200 ];then
       #确定nginx是宕机，关闭本机的keepalived
       service keepalived stop
     fi
   fi
   ```

5. 添加权限：chmod +x /home/check_nginx.sh，并执行脚本检查

   ```
   [root@nginx1 home]# chmod +x /home/check_nginx.sh 
   [root@nginx1 home]# sh -x check_nginx.sh 
   + url=http://127.0.0.1/check.html
   ++ curl -s -o /dev/null -w '%{http_code}' http://127.0.0.1/check.html
   + code=404
   + '[' 404 -ne 200 ']'
   + sleep 1
   ++ curl -s -o /dev/null -w '%{http_code}' http://127.0.0.1/check.html
   + code=404
   + '[' 404 -ne 200 ']'
   + service keepalived stop
   keepalived: unrecognized service
   ```

6. 在 nginx1 上的/opt/nginx/html 目录创建一个 check.html

   ```
   [root@nginx1 home]# cd /opt/nginx/html/
   [root@nginx1 html]# vim check.html
   
   check nginx
   ```

7. 将 nginx1 上的/home/check_nginx.sh 拷贝到 nginx2 上,将nginx1上的/opt/nginx/html/check.html拷贝到nginx2上相同目录下

   ```
   [root@nginx1 home]# cd /opt/nginx/conf/
   [root@nginx1 conf]# scp nginx.conf nginx2:`pwd`
   root@nginx2's password: 
   nginx.conf                                                                  100% 1066     1.0KB/s   00:00    
   [root@nginx1 conf]# cd /opt/nginx/html/
   [root@nginx1 html]# ls
   50x.html  check.html  index.html
   [root@nginx1 html]# scp check.html nginx2:`pwd`
   root@nginx2's password: 
   check.html                                                                  100%   12     0.0KB/s   00:00 
   [root@nginx1 html]# cd /home/
   [root@nginx1 home]# scp check_nginx.sh nginx2:`pwd`
   root@nginx2's password: 
   check_nginx.sh                                                              100%  553     0.5KB/s   00:00  
   ```

8. 分别在nginx1上和nginx2上测试check_nginx.sh是否可以正确的检查出nginx是否宕机：

   nginx1：

   ```
   [root@nginx1 home]# service nginx restart
   nginx: the configuration file /opt/nginx/conf/nginx.conf syntax is ok
   nginx: configuration file /opt/nginx/conf/nginx.conf test is successful
   Stopping nginx:                                            [  OK  ]
   Starting nginx:                                            [  OK  ]
   [root@nginx1 home]# cd /home/
   [root@nginx1 home]# sh -x check_nginx.sh 
   + url=http://127.0.0.1/check.html
   ++ curl -s -o /dev/null -w '%{http_code}' http://127.0.0.1/check.html
   + code=200
   + '[' 200 -ne 200 ']'
   ```

   以上提示表示nginx正常服务,如果关闭nginx，则

   ```
   [root@nginx1 home]# service nginx stop
   Stopping nginx:                                            [  OK  ]
   [root@nginx1 home]# cd /home/
   [root@nginx1 home]# sh -x check_nginx.sh 
   + url=http://127.0.0.1/check.html
   ++ curl -s -o /dev/null -w '%{http_code}' http://127.0.0.1/check.html
   + code=000
   + '[' 000 -ne 200 ']'
   + sleep 1
   ++ curl -s -o /dev/null -w '%{http_code}' http://127.0.0.1/check.html
   + code=000
   + '[' 000 -ne 200 ']'    # 非200的值
   + service keepalived stop
   keepalived: unrecognized service
   ```

   nginx2：

   ```
   [root@nginx2 home]# service nginx restart
   nginx: the configuration file /opt/nginx/conf/nginx.conf syntax is ok
   nginx: configuration file /opt/nginx/conf/nginx.conf test is successful
   Stopping nginx:                                            [  OK  ]
   Starting nginx:                                            [  OK  ]
   [root@nginx2 home]# sh -x check_nginx.sh 
   + url=http://127.0.0.1/check.html
   ++ curl -s -o /dev/null -w '%{http_code}' http://127.0.0.1/check.html
   + code=200
   + '[' 200 -ne 200 ']'
   ```

   以上提示表示nginx正常服务,如果关闭nginx，则

   ```
   [root@nginx2 home]# service nginx stop
   Stopping nginx:                                            [  OK  ]
   [root@nginx2 home]# sh -x check_nginx.sh 
   + url=http://127.0.0.1/check.html
   ++ curl -s -o /dev/null -w '%{http_code}' http://127.0.0.1/check.html
   + code=000
   + '[' 000 -ne 200 ']'
   + sleep 1
   ++ curl -s -o /dev/null -w '%{http_code}' http://127.0.0.1/check.html
   + code=000
   + '[' 000 -ne 200 ']'  # 非200的值
   + service keepalived stop
   keepalived: unrecognized service
   ```

   以上提示表示 nginx 已经宕机了。

9. 在 nginx1 和 nginx2 上通过 yum 安装 keepalived

   nginx1:

   ```
   [root@nginx1 home]# yum install keepalived -y
   ```

   nginx2:

   ```
   [root@nginx2 home]# yum install keepalived -y
   ```

10. 在nginx1 上配置/etc/keepalived/keepalived.conf 文件如下

    ```
    [root@nginx1 home]# vim /etc/keepalived/keepalived.conf
    
    ! Configuration File for keepalived
    
    global_defs {
       #配置接收邮件的邮箱地址，指定 keepalived 在发生切换时需要发送 email 到的邮 箱地址，一行一个
       notification_email {
         gtjin@bjsxt.com
       }
       notification_email_from keepmanager@126.com  #指定发件人
       smtp_server 192.168.200.1  #指定邮件smtp服务器的地址
       smtp_connect_timeout 30    #指定smtp连接的超时时间
       router_id nginx1           #运行keepalived机器的一个标识
    }
    #手动定义一个检查机制
    vrrp_script chk_nginx {
       script "home/check_nginx.sh"
       interval  2  #每隔2秒检查一次
       #weight -20
    }
    
    vrrp_instance VI_1 {
       # 指定实例的初始化状态，两台路由器都启动后，马上会发生竞选。priority 优先级 高的被选为 Master
        #这里的 MASTER 并不能代表当前实例一直为MASTER
        state MASTER
        interface eth0   #实例绑定的网卡设备
        virtual_router_id 105 #VRID的标记（0~255）
        priority 100     #优先级 该值高的实例优先竞选为 MASTER，低的为 BACKUP
        nopreempt        #挂掉后再次启动后还做 MASTE
        advert_int 1     #检查间隔：默认为1秒
        #认证的位置
        authentication {
            auth_type PASS   #认证的方式 PASS或AH
            auth_pass 1111   #认证的密码
        }
        #指定虚拟ip地址，也是VIP
        virtual_ipaddress {
            192.168.236.200/24 dev eth0 label eth0:3
        }
        track_script {
            chk_nginx  #调用上面定义好的检测
        }
    }
    ```

    在此处，我中途优势，文件未闭关，我打开这个文件，碰到报错E325:ATTENTION

    解决办法如下：https://blog.csdn.net/qq_38329988/article/details/88668128

11. 将配置文件远程拷贝到 nginx2 上一份

    ```
    [root@nginx1 keepalived]# scp keepalived.conf nginx2:`pwd`
    root@nginx2's password: 
    keepalived.conf                                                             100% 1517     1.5KB/s   00:00    
    ```

12. 在nginx2 上修改 keepalived.conf 文件

    ```
       router_id nginx2           #运行keepalived机器的一个标识
    vrrp_instance VI_1 {
       # 指定实例的初始化状态，两台路由器都启动后，马上会发生竞选。priority 优先级 高的被选为 Master
        state BACKUP
        interface eth0   #实例绑定的网卡设备
        virtual_router_id 106 #VRID的标记（0~255）
        priority 90     #优先级 该值高的实例优先竞选为 MASTER，低的为 BACKUP
        advert_int 1     #检查间隔：默认为1秒
    ```

    ```
    [root@nginx2 ~]# vim /etc/keepalived/keepalived.conf 
    
    ! Configuration File for keepalived
    
    global_defs {
       #配置接收邮件的邮箱地址，指定 keepalived 在发生切换时需要发送 email 到的邮 箱地址，一行一个
       notification_email {
         gtjin@bjsxt.com
       }
       notification_email_from keepmanager@126.com  #指定发件人
       smtp_server 192.168.200.1  #指定邮件smtp服务器的地址
       smtp_connect_timeout 30    #指定smtp连接的超时时间
       router_id nginx2           #运行keepalived机器的一个标识
    }
    #手动定义一个检查机制
    vrrp_script chk_nginx {
       script "home/check_nginx.sh"
       interval  2  #每隔2秒检查一次
       #weight   -20
    }
    
    vrrp_instance VI_1 {
       # 指定实例的初始化状态，两台路由器都启动后，马上会发生竞选。priority 优先级 高的被选为 Master
        #这里的 MASTER 并不能代表当前实例一直为MASTER
        state BACKUP
        interface eth0   #实例绑定的网卡设备
        virtual_router_id 106 #VRID的标记（0~255）
        priority 90     #优先级 该值高的实例优先竞选为 MASTER，低的为 BACKUP
        advert_int 1     #检查间隔：默认为1秒
        #认证的位置
        authentication {
            auth_type PASS   #认证的方式 PASS或AH
            auth_pass 1111   #认证的密码
        }
        #指定虚拟ip地址，也是VIP
        virtual_ipaddress {
            192.168.236.200/24 dev eth0 label eth0:3
        }
        track_script {
            chk_nginx  #调用上面定义好的检测
        }
    }
    ```

13. 在nginx1 上启动 keepalived，并在nginx1 上检查

    ```
    [root@nginx1 keepalived]# service keepalived start
    Starting keepalived:                                       [  OK  ]
    
    # 如果memcached、nginx之前已启动，则重启一下
    [root@nginx1 keepalived]# service memcached start
    Starting memcached:                                        [  OK  ]
    [root@nginx1 keepalived]# service nginx start
    Starting nginx:                                            [  OK  ]
    
    [root@nginx1 keepalived]# ifconfig
    eth0      Link encap:Ethernet  HWaddr 00:0C:29:F6:9E:AD  
              inet addr:192.168.236.105  Bcast:192.168.236.255  Mask:255.255.255.0
              inet6 addr: fe80::20c:29ff:fef6:9ead/64 Scope:Link
              UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
              RX packets:13869 errors:0 dropped:0 overruns:0 frame:0
              TX packets:6390 errors:0 dropped:0 overruns:0 carrier:0
              collisions:0 txqueuelen:1000 
              RX bytes:1269953 (1.2 MiB)  TX bytes:991529 (968.2 KiB)
    
    eth0:3    Link encap:Ethernet  HWaddr 00:0C:29:F6:9E:AD  
              inet addr:192.168.236.200  Bcast:0.0.0.0  Mask:255.255.255.0
              UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
    
    lo        Link encap:Local Loopback  
              inet addr:127.0.0.1  Mask:255.0.0.0
              inet6 addr: ::1/128 Scope:Host
              UP LOOPBACK RUNNING  MTU:16436  Metric:1
              RX packets:233 errors:0 dropped:0 overruns:0 frame:0
              TX packets:233 errors:0 dropped:0 overruns:0 carrier:0
              collisions:0 txqueuelen:0 
              RX bytes:20544 (20.0 KiB)  TX bytes:20544 (20.0 KiB)
    ```

14. 在nginx2 上启动 keepalived ，并进行检查

    ```
    [root@nginx2 keepalived]# service keepalived start
    Starting keepalived:                                       [  OK  ]
    
    # 如果memcached、nginx之前已启动，则重启一下
    [root@nginx2 keepalived]# service memcached start
    Starting memcached:                                        [  OK  ]
    [root@nginx2 keepalived]# service nginx start
    Starting nginx:                                            [  OK  ]
    
    [root@nginx2 ~]# ifconfig
    eth0      Link encap:Ethernet  HWaddr 00:0C:29:7C:42:42  
              inet addr:192.168.236.106  Bcast:192.168.236.255  Mask:255.255.255.0
              inet6 addr: fe80::20c:29ff:fe7c:4242/64 Scope:Link
              UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
              RX packets:6622 errors:0 dropped:0 overruns:0 frame:0
              TX packets:1627 errors:0 dropped:0 overruns:0 carrier:0
              collisions:0 txqueuelen:1000 
              RX bytes:511534 (499.5 KiB)  TX bytes:205553 (200.7 KiB)
    
    lo        Link encap:Local Loopback  
              inet addr:127.0.0.1  Mask:255.0.0.0
              inet6 addr: ::1/128 Scope:Host
              UP LOOPBACK RUNNING  MTU:16436  Metric:1
              RX packets:44 errors:0 dropped:0 overruns:0 frame:0
              TX packets:44 errors:0 dropped:0 overruns:0 carrier:0
              collisions:0 txqueuelen:0 
              RX bytes:3682 (3.5 KiB)  TX bytes:3682 (3.5 KiB)
    
    ```

    发现：在nginx2上，没有启动 eth0:3 的网卡 

15. 启动node2，node3上的tomcat

    ```
    [root@node2 ~]# cd /opt/tomcat-7.0.69/bin/
    [root@node2 bin]# ./startup.sh 
    Using CATALINA_BASE:   /opt/tomcat-7.0.69
    Using CATALINA_HOME:   /opt/tomcat-7.0.69
    Using CATALINA_TMPDIR: /opt/tomcat-7.0.69/temp
    Using JRE_HOME:        /usr/java/jdk1.7.0_80
    Using CLASSPATH:       /opt/tomcat-7.0.69/bin/bootstrap.jar:/opt/tomcat-7.0.69/bin/tomcat-juli.jar
    Tomcat started.
    ```

    ```
    [root@node3 ~]# cd /opt/tomcat-7.0.69/bin/
    [root@node3 bin]# ./startup.sh 
    Using CATALINA_BASE:   /opt/tomcat-7.0.69
    Using CATALINA_HOME:   /opt/tomcat-7.0.69
    Using CATALINA_TMPDIR: /opt/tomcat-7.0.69/temp
    Using JRE_HOME:        /usr/java/jdk1.7.0_80
    Using CLASSPATH:       /opt/tomcat-7.0.69/bin/bootstrap.jar:/opt/tomcat-7.0.69/bin/tomcat-juli.jar
    Tomcat started.
    ```

    此时，在网页上访问：http://192.168.236.200/

    ![image-20211217135354945](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112171353156.png)

    ![image-20211217135454244](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112171354363.png)

16. 将 nginx1 上的 nginx down 掉并测试

    ```
    [root@nginx1 keepalived]# service nginx stop
    Stopping nginx:                                            [  OK  ]
    [root@nginx1 keepalived]# ifconfig
    eth0      Link encap:Ethernet  HWaddr 00:0C:29:F6:9E:AD  
              inet addr:192.168.236.105  Bcast:192.168.236.255  Mask:255.255.255.0
              inet6 addr: fe80::20c:29ff:fef6:9ead/64 Scope:Link
              UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
              RX packets:14866 errors:0 dropped:0 overruns:0 frame:0
              TX packets:7842 errors:0 dropped:0 overruns:0 carrier:0
              collisions:0 txqueuelen:1000 
              RX bytes:1396809 (1.3 MiB)  TX bytes:1144748 (1.0 MiB)
    
    lo        Link encap:Local Loopback  
              inet addr:127.0.0.1  Mask:255.0.0.0
              inet6 addr: ::1/128 Scope:Host
              UP LOOPBACK RUNNING  MTU:16436  Metric:1
              RX packets:5121 errors:0 dropped:0 overruns:0 frame:0
              TX packets:5121 errors:0 dropped:0 overruns:0 carrier:0
              collisions:0 txqueuelen:0 
              RX bytes:450536 (439.9 KiB)  TX bytes:450536 (439.9 KiB)
    ```

    此时，在nginx2上测试，可以看到：

    ```
    [root@nginx2 ~]# ifconfig
    eth0      Link encap:Ethernet  HWaddr 00:0C:29:7C:42:42  
              inet addr:192.168.236.106  Bcast:192.168.236.255  Mask:255.255.255.0
              inet6 addr: fe80::20c:29ff:fe7c:4242/64 Scope:Link
              UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
              RX packets:7058 errors:0 dropped:0 overruns:0 frame:0
              TX packets:1743 errors:0 dropped:0 overruns:0 carrier:0
              collisions:0 txqueuelen:1000 
              RX bytes:544096 (531.3 KiB)  TX bytes:223289 (218.0 KiB)
    
    eth0:3    Link encap:Ethernet  HWaddr 00:0C:29:7C:42:42  
              inet addr:192.168.236.200  Bcast:0.0.0.0  Mask:255.255.255.0
              UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
    
    lo        Link encap:Local Loopback  
              inet addr:127.0.0.1  Mask:255.0.0.0
              inet6 addr: ::1/128 Scope:Host
              UP LOOPBACK RUNNING  MTU:16436  Metric:1
              RX packets:128 errors:0 dropped:0 overruns:0 frame:0
              TX packets:128 errors:0 dropped:0 overruns:0 carrier:0
              collisions:0 txqueuelen:0 
              RX bytes:11074 (10.8 KiB)  TX bytes:11074 (10.8 KiB)
    ```

    发现：在nginx2上启动了eth0:3网卡

    此时，在网页测试：http://192.168.236.200/

    ![image-20211217140236363](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112171402747.png)

    ![image-20211217140247567](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112171402796.png)

    发现：在nginx2服务器上仍然可以做转发与负载均衡

17. 如果将nginx1 上的 nginx 和 keepalived 启动起来，

    ```
    [root@nginx1 keepalived]# service nginx start
    Starting nginx:                                            [  OK  ]
    [root@nginx1 keepalived]# service keepalived start
    Starting keepalived:                                       [  OK  ]
    [root@nginx1 keepalived]# ifconfig
    eth0      Link encap:Ethernet  HWaddr 00:0C:29:F6:9E:AD  
              inet addr:192.168.236.105  Bcast:192.168.236.255  Mask:255.255.255.0
              inet6 addr: fe80::20c:29ff:fef6:9ead/64 Scope:Link
              UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
              RX packets:15279 errors:0 dropped:0 overruns:0 frame:0
              TX packets:7965 errors:0 dropped:0 overruns:0 carrier:0
              collisions:0 txqueuelen:1000 
              RX bytes:1429005 (1.3 MiB)  TX bytes:1158548 (1.1 MiB)
    
    eth0:3    Link encap:Ethernet  HWaddr 00:0C:29:F6:9E:AD  
              inet addr:192.168.236.200  Bcast:0.0.0.0  Mask:255.255.255.0
              UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
    
    lo        Link encap:Local Loopback  
              inet addr:127.0.0.1  Mask:255.0.0.0
              inet6 addr: ::1/128 Scope:Host
              UP LOOPBACK RUNNING  MTU:16436  Metric:1
              RX packets:5157 errors:0 dropped:0 overruns:0 frame:0
              TX packets:5157 errors:0 dropped:0 overruns:0 carrier:0
              collisions:0 txqueuelen:0 
              RX bytes:453704 (443.0 KiB)  TX bytes:453704 (443.0 KiB)
    ```

    发现：在nginx1上又重新启动了eth0:3网卡，因为我们在nginx1中设置了挂掉后再次启动后还做 MASTER。

    ```
    [root@nginx2 ~]# service nginx stop
    Stopping nginx:                                            [  OK  ]
    [root@nginx2 ~]# ifconfig
    eth0      Link encap:Ethernet  HWaddr 00:0C:29:7C:42:42  
              inet addr:192.168.236.106  Bcast:192.168.236.255  Mask:255.255.255.0
              inet6 addr: fe80::20c:29ff:fe7c:4242/64 Scope:Link
              UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
              RX packets:7549 errors:0 dropped:0 overruns:0 frame:0
              TX packets:2437 errors:0 dropped:0 overruns:0 carrier:0
              collisions:0 txqueuelen:1000 
              RX bytes:581158 (567.5 KiB)  TX bytes:268835 (262.5 KiB)
    
    lo        Link encap:Local Loopback  
              inet addr:127.0.0.1  Mask:255.0.0.0
              inet6 addr: ::1/128 Scope:Host
              UP LOOPBACK RUNNING  MTU:16436  Metric:1
              RX packets:3538 errors:0 dropped:0 overruns:0 frame:0
              TX packets:3538 errors:0 dropped:0 overruns:0 carrier:0
              collisions:0 txqueuelen:0 
              RX bytes:311074 (303.7 KiB)  TX bytes:311074 (303.7 KiB)
    ```

    