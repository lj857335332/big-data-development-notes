[TOC]



# linux操作系统概述与安装

　　常规开发很多时候都是在windows系统下开发，然后在Linux系统下做部署。而大数据开发，很多时候我们需要使用虚拟机模拟Linux系统，将集群搭建在Linux系统里面。所以，我们需要学习Linux系统如何来使用，下面先对操作系统做一些了解！

------



## 1.操作系统

　　Linux 系统的核心是内核，内核控制着计算机系统上的所有硬件和软件，在必要时分配硬件，并根据需要执行软件。 内核主要负责以下四种功能：系统内存管理、软件程序管理、硬件设备管理、文件系统管理。

|           | 介绍                                   |
| --------- | -------------------------------------- |
| 内核特点  | 支持多用户、多进程、多线程和多CPU      |
| 应用领域  | 智能设备、后台服务器、云计算、大数据等 |
| Linux家族 | Ubuntu、CentOS、RedHat、Android 等     |

- Red Hat：目前被 IBM 收购了。收费版。目前全球最大的 Linux 供应商。 

- CentOS：Red Hat 推出的免费版

- Ubuntu：界面比较友好。

## 2.安装vmware软件

### 2.1 vmware简介

　　VMWare 就是虚拟机软件。使用 VMWare 就是使用软件来模拟一台真实的计算机。由于虚拟机安装在当前计算机中，所以虚拟机硬件配置上限就是当前计算机硬件配置。对于电脑内存配置来说，至少8G（越大越好）。

![image-20211120222821454](https://s2.loli.net/2021/12/04/fyoEnZgKkhRVmT7.png)

### 2.2 安装步骤

1. 运行程序进行安装

![image-20211121164634873](https://s2.loli.net/2021/12/04/vVWzthHwEDOa4jy.png)

2. 接受许可协议条款，并下一步

   ![image-20211121164754328](https://s2.loli.net/2021/12/04/9pntyvRQiEk4eHq.png)

3. 更改安装位置至非系统盘

   ![image-20211121165120181](https://s2.loli.net/2021/12/04/2EtMQDHJoNYqWCs.png)

4. 不勾选，下一步

![image-20211121165154269](https://s2.loli.net/2021/12/04/Y4dVexTJzCvlFEh.png)

5. 在桌面和开始菜单创建快捷方式，并进行安装

![image-20211121165317634](https://s2.loli.net/2021/12/04/fKJTWn83iN15XPC.png)

6. 点击输入许可证信息，最后点击"完成"，完成安装

   ![image-20211121165626944](https://s2.loli.net/2021/12/04/yIKsdVPNtlXEwYQ.png)

### 2.3 卸载步骤

　　VMWare 使用 windows 的程序卸载可能无法卸载干净。要使用安装包的卸载功能进行卸载。双击安装包后点击下一步， 出现下面的界面，点击“删除按钮进行删除

![image-20211121170034197](https://s2.loli.net/2021/12/04/SXWa9ul16Q5GhPB.png)

## 3. 新建虚拟机

1. 启动vmware软件![image-20211121170341284](https://s2.loli.net/2021/12/04/7EIj91LvTwYoFAt.png)

2. 选择新建虚拟机，可以点击上图中的“创建新的虚拟机”或者点击“文件”菜单

3. 打开创建向导，选择自定义，点击“下一步“

   ![image-20211121170636978](https://s2.loli.net/2021/12/04/KEV3AL41viXSqzZ.png)

4. 点击“下一步”，选择虚拟机兼容性，默认兼容“WorkStation12.x”

![image-20211121170736897](https://s2.loli.net/2021/12/05/3y62hZFVG1CJuEN.png)

5. 点击“下一步”，选择“稍后安装操作系统“

![image-20211121170816066](https://s2.loli.net/2021/12/05/QDjgsMp8CYyfwA9.png)

6. 点击下一步，选择“Linux”，在版本中选择“CentOS 6 64 位”

![image-20211121171938993](https://s2.loli.net/2021/12/04/5KFRGOT9aPdWDyB.png)

7. 点击“下一步”，“虚拟机名称”填写你想要的名称，该名称是在 VMware 中用于区分不同虚拟机的名字，“位置”指的是将新创建的虚拟机文件安装到哪个目录，建议在选择该目录的时候要选一个磁盘分区空间大的，同时对每个虚拟机创建二级目录

![image-20211121172216862](https://s2.loli.net/2021/12/05/KcFLuQgyJiaANVO.png)

8. 点击“下一步”，设置虚拟机处理器的个数，以及每个处理器的内核数，这里采用默认的设置，1 个处理器，2个核心

![image-20211121172323153](https://s2.loli.net/2021/12/05/kCperLGbTJoVjyz.png)

9.点击“下一步”，设置虚拟机的内存，这里采用默认的 1024MB，1GB 内存。如果内存小的话，虚拟机操作系统的安装就变成了文字格式，1GB 内存可以使用图形界面安装虚拟机操作系统。

![image-20211121172456989](https://s2.loli.net/2021/12/05/oE8Xj2OZdHT1VLJ.png)

10.点击“下一步”，选择虚拟机的网络类型，这里选择 NAT 模式，虚拟机的上网由 Windows 宿主机代理上网

![image-20211121172559702](https://s2.loli.net/2021/12/05/tQgTpo6WxZ7Ij8k.png)

11. 点击“下一步”，选择 I/O 控制器类型，使用默认值

![image-20211121172646979](https://s2.loli.net/2021/12/04/TGNnJzpruiFqICb.png)

12. 点击“下一步”，选择磁盘类型，使用默认值“SCSI”

    ![image-20211121172737276](https://s2.loli.net/2021/12/04/t13PSqfsVZgOmlG.png)

13. 点击“下一步”，使用默认值“创建新虚拟磁盘”

    ![image-20211121172819841](https://s2.loli.net/2021/12/04/dh4pGtoQVjzuWPx.png)

14. 点击“下一步”，设置虚拟机磁盘的大小，这里设置为 20GB，由于不勾选“立即分配 所有磁盘空间”，该 20GB 不会立即在磁盘占用，而会随着向虚拟机中创建和添加文件而逐 渐增大。选择“将虚拟磁盘存储为单个文件”以提高性能。

![image-20211121172953330](https://s2.loli.net/2021/12/04/ACnSuB5Ee1DwdbJ.png)

15. 点击“下一步”，给虚拟磁盘取名，使用默认值就可以

![image-20211121173032377](https://s2.loli.net/2021/12/04/tgLNYjA5Q6wJxiv.png)

16. 点击“下一步”，列出新建虚拟机的信息

![image-20211121173140059](https://s2.loli.net/2021/12/04/W2nQl81MSUigB5v.png)

17. 点击完成，搞定![image-20211121173225954](https://s2.loli.net/2021/12/04/794ThDgFsuxEpmK.png)

## 4. 安装 centos6.5 操作系统

### 4.1 安装步骤

1. 在 CentOS 6 64 位标签，点击“CD/DVD(IDE) 自动检测”条目，给虚拟机的光驱塞进去一张光盘，光盘通过 ISO 文件模拟![image-20211121184352107](https://s2.loli.net/2021/12/05/QSH3BsRImX16TLx.png)

2. 点击“浏览”，在查找对话框中找到“CentOS-6.5-x86_64-minimal.iso”， 点击“打开”，然后再点击确定即可![image-20211121184733769](https://s2.loli.net/2021/12/05/Q9TKIGwtmvZsy3X.png)

3. 点击“开启此虚拟机“![image-20211121184938344](https://s2.loli.net/2021/12/05/aUBywtRW6hZGcsJ.png)

4. 鼠标点击屏幕，使用“↑”“↓”键选择条目，此处选择第一个条目，键盘点击“回车”键![image-20211121185238669](https://s2.loli.net/2021/12/05/Hk6fixnu9o4qac3.png)
5. 使用“←”“→”键选择“Skip”，跳过磁盘检查并回车![image-20211121185106622](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121185106622.png)

6. 在该页面直接回车

   ![image-20211121185358750](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121185358750.png)

7. 进入 CentOS 图形安装界面，此时点击右下角的“Next”

   ![image-20211121185443695](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121185443695.png)

8. 语言选择“English(English)”并点击右下角的“Next”![image-20211121185537000](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121185537000.png)

9. 键盘选择“U.S. English”美式英语键盘，并点击右下角的“Next”

   ![image-20211121185631843](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121185631843.png)

10. 选择磁盘的类型，此处使用默认值（Basic Storage Devices），并点击右下角的“Next”![image-20211121185719690](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121185719690.png)

11. 由于需要对磁盘进行分区，此处提醒表示是否保留磁盘上的数据，因为是第一次使用， 我们点击“Yes, discard any data”![image-20211121185819367](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121185819367.png)

12. 要求我们配置虚拟机的操作系统主机名和网络，此处不需要做任何操作，直接点击“Next”![image-20211121185917029](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121185917029.png)

13. 选择时区，我们选择“Asia/Shanghai”，点击右下角“Next”![image-20211121190102847](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121190102847.png)

14. 设置 root 用户密码，root 用户拥有管理整个系统的权限，此密码一般设置成简单的。 我们设置为“123456”，输入两遍，然后点击右下角的“Next”

    ![image-20211121190223206](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121190223206.png)	

15. 提示密码太简单，点击“Use Anyway”![image-20211121190320719](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121190320719.png)

16. 选择磁盘分区方式，此处选择最后一个“Create Custom Layout”，自定义磁盘分 区，然后点击右下角的“Next”

​	![image-20211121190637384](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121190637384.png)

17. 点击“Create”创建新的分区

​	![image-20211121190734914](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121190734914.png)

18.创建标准分区，选择第一个“Standard Partition”，然后点击右下角的“Create”

​														![image-20211121190940141](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121190940141.png)

19. 选择挂载点“/boot”，文件类型“ext4”，大小“200”单位是 MB，选择“Fixed Size” (确定大小)，点击右下角的“OK”创建第一个分区

    ![image-20211121191145047](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121191145047.png)

20. 第一个分区创建完毕，可以在图中看到我们创建的/boot 分区，大小 200MB，格式为 ext4 

    ![image-20211121191238509](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121191238509.png)

21. 点击“Create”创建第二个分区；选择标准分区，点击右下角“Create”，在文件系统类型一栏选择“swap”（交换分区），设 置大小为 2048MB，准确大小，点击右下角的“OK”创建分区

    ![image-20211121191453825](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121191453825.png)

22. 交换分区创建完成，点击“Create”添加新的分区；创建标准分区，点击“Create”进行新分区的设置；将新分区挂载到“/”（根分区），文件类型设置为“ext4”，大小选择磁盘剩余总容量，点击 “OK”创建该分区

​															![image-20211121191747590](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121191747590.png)

23.可以看到新创建的分区已经创建成功，点击右下角的“Next”

​			![image-20211121191858330](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121191858330.png)

24. 提示磁盘需要格式化，点击右下角的“Format”格式化

    ![image-20211121191958435](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121191958435.png)

25. 点击“Write changes to disk”进行格式化；格式化结束后，提示在/dev/sda 设备安装 boot loader，用于启动/dev/sda3 上的 CentOS 系统，点击右下角“Next”

    ![image-20211121192302280](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121192302280.png)

26. 开始安装系统，一共有 205 个包需要安装，等待安装结束

    ![image-20211121192347030](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121192347030.png)

27. 系统安装完成，点击右下角的“Reboot”重启虚拟机，进入虚拟机操作系统

    ![image-20211121192506399](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121192506399.png)

28. 输入用户名（root），回车，输入密码（123456），回车进入系统

    需要注意的是，此时输入用户名 root 有提示，输入密码没有提示

    ![image-20211121192711392](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121192711392.png)

### 4.2 网络配置

1. 在命令提示符输入，

   ```
   vi /etc/sysconfig/network
   ```

   设置计算机名称

   ![image-20211121193030141](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121193030141.png)

   命令写完后，回车

2. 按小写的 i 进入编辑模式，可以看到最后一行的提示变为了“-- INSERT--“

   ![image-20211121193359821](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121193359821.png)

   3. 修改 HOSTNAME，修改完之后，按 ESC 键（键盘最左上角的）退出编辑模式

      ![image-20211121193529228](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121193529228.png)

   4. 此时 vi 在命令模式，输入“:wq”保存并退出。

      ![image-20211121193555481](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121193555481.png)

      在下图中可以看到 written 表示保存成功，同时在最后一行看到了熟悉的命令提示符。

      ![image-20211121193724420](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121193724420.png)

5. 在命令提示符输入（注意空格）

   ```
   vi /etc/sysconfig/network-scripts/ifcfg-eth0
   ```

​		编辑网络配置文件，回车，进入文件编辑： 

- [ ] 第一行表示硬件的名称，eth0 表示第一块有线网卡 

- [ ] 第二行表示该有线网卡的 MAC 地址 

- [ ] 第三行表示该网卡的类型为以太网卡 

- [ ] 第四行表示该网卡设备的编号，操作系统中每个设备都有一个唯一的编号 

- [ ] 第五行表示该网卡是否随系统启动而启动 

- [ ] 最后一行表示网卡连接网络的类型，默认是 dhcp 

![image-20211121210106941](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121210106941.png)

6. 设置 IP 地址；

   查看虚拟网络编辑器

​		![image-20211121210244421](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121210244421.png)

​		选择更改设置

​		![image-20211121210335516](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121210335516.png)

​		选择 VMnet8，可以看到该选项为 NAT 模式，子网 IP 为“192.168.236.0”，子网 掩码是“255.255.255.0”

​		![image-20211121210625803](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121210625803.png)

​		也可以点击“NAT 设置”查看详情：

​		![image-20211121210758442](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121210758442.png)

​		可见上图中，子网 IP 是“192.168.236.0”，子网掩码是“255.255.255.0”，网关地址为 “192.168.236.2”。所以我们的配置就是：

​		![image-20211121213430550](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121213430550.png)

​		上图中删除了 MAC 地址行和 UUID 行，同时将 ONBOOT 修改为 yes，随系统启动， BOOTPROTO 修改为 static，表示是静态 IP 地		址配置，IPADDR 表示配置的 IP 地址， NETMASK 为子网掩码，GATEWAY 为网关地址，DNS1 表示是 DNS 解析地址，DNS2 是备		用DNS 解析地址。

​		按 ESC 键退出编辑模式，输入`:wq`保存退出

​		![image-20211121213651656](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121213651656.png)

7. 在 命 令 提 示 符 ， 输 入`service network restart`重 启 网 络 ， 然 后 输 入`ping www.baidu.com`ping 一下百度网址，如果连接，表示网络配置成功

   ![image-20211121213534122](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121213534122.png)

​		Linux 中的 ping 默认不会停止，可以使用 `Ctrl+C` 停止该命令的执行，也就是 windows 中的复制的快捷

8. 输入`vi /etc/hosts`编辑 hosts 文件，添加本地解析条目

   ![image-20211122162333410](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122162333410.png)

​		此时域名 node0、node1、node2、node3、node4 会被解析为配置的 IP 地址。 保存退出。

​		此时，在 ping node0 的时候，node0 解析为了 192.168.20.100

​		![image-20211121214738555](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121214738555.png)

## 5. 安装 Xshell5.exe 和 Xftp5.exe

​	![image-20211121214921090](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121214921090.png)

​	这两个软件安装比较简单，需要注意，一定要选择家庭/教育，不要选择商业用途。

​	![image-20211121215418968](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121215418968.png)

​	![image-20211121220128592](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121220128592.png)

​	Xshell5软件用于远程连接（远程登录）虚拟机操作系统；Xftp5软件用于远程上传与下载文件

### 5.1 远程登录Linux系统

1. 点击文件菜单下的新建，名称：test，协议选择SSH，主机填CentOS系统指定的IP：192.168.236.100

   ![image-20211121221234913](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121221234913.png)

   

2. 点击连接，并输入用户名与密码

​									![image-20211121221324466](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121221324466.png)

​									![image-20211121221429085](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121221429085.png)

​									![image-20211121221454204](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121221454204.png)

3. 远程登录到Linux系统后就可以在，Xshell中通过指令来远程操作Linux系统

![image-20211121221608326](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121221608326.png)

​		然后使用 `ifconfig` 看一下网络配置：

​		![image-20211121221835483](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121221835483.png)

​		表明网络配置生效，ping 百度试一下

​		![image-20211121221940252](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211121221940252.png)

​		ping 通了百度。

## 6.其他配置

　　其他配置包括安装 ntp、vim、关闭防火墙、关闭 selinux、sshd 服务禁用 DNS，配置时间同步、删除 70-persistent-net.rules 文件以及最后的拍摄快照

### 6.1 配置本地 yum 源

　　没有配置本地 yum 源的时候，安装 vim 出现如下错误提示：

​		![image-20211122104400000](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122104400000.png)

​		这里我们选择使用本地 yum 源。需要在 VMware中设置下连接光驱（这里的ISO映像文件选择DVD版本，相较于min版本的ISO映像		文件多了许多第三方的RPM包），步骤如下图：

​		![image-20211122105809455](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122105809455.png)

- [ ] 每台节点创建/mnt/cdrom 目录

```
[root@node0 ~]# mkdir /mnt/cdrom
```

- [ ] 每台节点执行如下命令，将光盘设备/dev/sr0 挂载到/mnt/cdrom 目录

```
[root@node0 mnt]# mount /dev/cdrom /mnt/cdrom
mount: block device /dev/sr0 is write-protected, mounting read-only
```

- [ ] 每台节点执行命令:`df -h` 查看是否挂载

![image-20211122112557747](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122112557747.png)

- [ ] 上面的 mount 命令挂载后在机器重启后会失效，为了可以**实现开机自动挂载**，可 以在每台节点的/etc/fstab 文件的最后面加入下面语句：

  ```
  [root@node0 mnt]# vi /etc/fstab
  ```

​		![image-20211122112917076](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122112917076.png)下面可以创建本地 yum 源，在每台节点的/etc/yum.repos.d 目录下创建 local.repo 文件，

​		![image-20211122113705320](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122113705320.png)

​		内容如下：

​		![image-20211122113629388](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122113629388.png)

以上内容中，baseurl 是指 Yum 源的地址，该路径下有个 repodata 目录，就是 yum 安装源目录。file://表示 yum 源为文件

如果只想让 local.repo 生效，可以在每台节点的/etc/yum.repos.d 目录下 创建一个 backup 目录，将其他的以“.repo”结尾的文件都移动到 backup 目录 中。

```
[root@node0 yum.repos.d]# mkdir backup
[root@node0 yum.repos.d]# mv CentOS-* backup/
[root@node0 yum.repos.d]# ls
backup  local.repo
```

在每台节点上执行以下命令，更新 yum 源

```
[root@node0 yum.repos.d]# yum clean all
Loaded plugins: fastestmirror
Cleaning repos: local
Cleaning up Everything
[root@node0 yum.repos.d]# yum makecache
Loaded plugins: fastestmirror
Determining fastest mirrors
local                                                                        | 4.0 kB     00:00 ... 
local/group_gz                                                               | 226 kB     00:00 ... 
local/filelists_db                                                           | 6.3 MB     00:00 ... 
local/primary_db                                                             | 4.7 MB     00:00 ... 
local/other_db                                                               | 2.8 MB     00:00 ... 
Metadata Cache Created
```

此时，yum源就可以正常使用了

![image-20211122114720771](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122114720771.png)

### 6.2 安装 ntp 和 vim

​		在命令提示符中输入：`yum install vim ntp`安装 ntp 和 vim，

​		![image-20211122134715549](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122134715549.png)

​		最上面两行表示要安装的软件，中间的部分表示要安装的依赖，有点儿类似于 java 中的添 加 jar 包依赖。倒数第三行表示需要下载 		20MB 的数据，倒数第二行表示安装完成后占用磁 盘 61MB 的空间，最后提示是否可以，输入“y”回车，开始下载并安装

​		![image-20211122134837460](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122134837460.png)

​		输入 y 回车之后，yum 会从网上的软件源下载需要的 rpm 包到本地，执行检查，在最后一 行输入“y”回车并开始安装，安装完之后		显示：

​		![image-20211122134943286](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122134943286.png)

### 6.3 关闭防火墙并禁止开机启动

1. 输入 `service iptables status` 查看防火墙的状态

​		![image-20211122135237061](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122135237061.png)

2. 上图显示防火墙在运行状态，输入命令：`service iptables stop` 以关闭防火墙

   ![image-20211122135421139](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122135421139.png)

3. 禁止开机启动

   ![image-20211122135627030](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122135627030.png)

​			输入 `chkconfig`命令并回车，查看 iptables 条目，发现在运行级别 2，3，4，5 是 on，其 中 3 表示文字方式启动，5 表示图形界面方式启动

​			![image-20211122140115268](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122140115268.png)

​			输入命令：`chkconfig iptables off`全部禁止启动：

​			![image-20211122140249207](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122140249207.png)

​			禁止开机启动成功。防火墙服务以及防火墙开机启动都关闭成功。

### 6.4 禁用 selinux

​		selinux 是一个增强的 linux 安全子系统，我们不需要这个服务，关闭掉。

1. 在命令行输入：`getenforce`， 查看 selinux 的状态

   ![image-20211122140532493](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122140532493.png)

2. 在命令行输入：`vim /etc/selinux/config`编辑 selinux 的配置文件

​		![image-20211122140708225](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122140708225.png)

​		将上图中的 SELINUX=enforcing 修改为：

​		![image-20211122140822983](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122140822983.png)

​		保存退出即可。需要重启虚拟机才能生效。

### 6.5 关闭 sshd 服务的 DNS 以加快SSH登录速度

1. 在命令行输入：`vim /etc/ssh/sshd_config`，并回车

​		![image-20211122142654961](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122142654961.png)

2. 打开文件后在命令模式输入`/UseDNS`（注意大小写），查找 UseDNS 选项

   ![image-20211122143312218](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122143312218.png)

​		将注释起来的“UseDNS yes”注释放开，并将 yes 改为no

![image-20211122143419523](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122143419523.png)

​		保存退出，并重启 sshd 服务

![image-20211122143552209](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122143552209.png)

​		重启 sshd 服务之后配置项就加载了

### 6.6 时间同步

​		在安装完 Linux 操作系统之后，输入：`date`查看当前时间：

​		![image-20211122143828186](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122143828186.png)

​		时间不对。安装完 ntp 之后系统中多了两个服务：ntpd 和 ntpdate。对于 ntpd，命令 chkconfig 打印它的状态都是 off 的，也就是		这个服务不会开机启动。

​		![image-20211122144105690](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122144105690.png)

​		在命令行输入： `chkconfig ntpd on`，再次通过输入： `chkconfig` 查看

​		![image-20211122144300204](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122144300204.png)

​		在命令行输入：`service ntpd status`查看 ntpd 服务状态，如果显示 stopped ，表示没有启动，此时输入：`service ntpd start`启动服务，过几分钟时间就通过网络自动同步到当前时间了。 可以通过 `date` 命令查看同步之后的时间

​		![image-20211122144615286](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122144615286.png)

​		到此就配置了 ntpd 的开机启动和自动同步网络时间。

### 6.7 删除 70-persistent-net.rules文件

​		输入命令：<u>`rm -rf /etc/udev/rules.d/70-persistent-net.rules`</u>

​		![image-20211122145348780](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122145348780.png)

​		![image-20211122145028937](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122145028937.png)

​		![image-20211122145152094](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122145152094.png)

## 7. 拍摄快照

​		![image-20211122150156059](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122150156059.png)

​		如上如，要给虚拟机拍摄快照，点击 1 快速拍快照，点击 2 可以查看、删除、拍摄快照。

​		我们点击 1：

![image-20211122150421153](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122150421153.png)

​		依次点击 1，在 2 处输入快照名称，点击3拍摄快照。还可以在描述中添加虚拟机快照描述信息。

​		![image-20211122150708334](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122150708334.png)

​		点击上图的 2，查看具体的快照信息进行快照管理：

​		![image-20211122150801475](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122150801475.png)

​		1 处显示刚才拍摄的快照，2 处是当前的状态，3 处可以拍摄快照，4 处可以在各个快照之 间切换，5 处可以删除快照。

​		需要注意：为了防止克隆的虚拟机连不上网，使用初始状态进行克隆。

## 8. 克隆虚拟机

​		上节搭建的虚拟机只是一个模板，同时也是所有链接克隆虚拟机的基础，没有了这个基础， 链接克隆的虚拟机都将不能使用。但是链接克隆一个好处是克隆出来的虚拟机不会占用整个 虚拟机的磁盘空间，同时链接克隆速度比较快，不用经历安装虚拟机要命的超长等待时间。

![image-20211122151239618](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122151239618.png)

​			![image-20211122151319122](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122151319122.png)

​			![image-20211122151351911](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122151351911.png)

​			![image-20211122151436724](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122151436724.png)

​			![image-20211122151902996](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122151902996.png)

​			我们需要克隆四台虚拟机，也就是node1、 node2、node3、node4

​			![image-20211122152050333](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122152050333.png)

​			开启 node1、node2、node3、node4 四台虚拟机，分别配置网络 IP 地址和计算机名称 hostname

​			使用原来的用户名 root 和密码 123456 登录虚拟机，输入：`vim /etc/sysconfig/network`，修改计算机名称（hostname）

​			![image-20211122152456344](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122152456344.png)

​			输入：`vim /etc/sysconfig/network-scripts/ifcfg-eth0`，配置网络 IP 地址

​			![image-20211122152803778](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122152803778.png)

​			将 node1 的 IP 地址修改为 192.168.20.101，保存退出。

​			重启 node1 虚拟机之后，使用Xshell连接：

​			![image-20211122153250274](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122153250274.png)

​			在 windows 的 C:\Windows\System32\drivers\etc\hosts 文件中追加如下内容：

​			![image-20211122162143576](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122162143576.png)

​			注意： 在四台虚拟机配置好网络和计算机名称，需要拍摄快照，作为初始快照，步骤如拍摄快照。切记！！！