[TOC]



# grep正则表达式与shellscipt脚本编程

## 1.grep之正则表达式

&emsp;&emsp;grep命令含义是打印匹配给定模型的行，grep 搜索以 “FILE” 命名的文件输入 ，寻找含有与给定的模式 PATTERN 相匹配的内容的行。默认情况下， grep 将把含有匹配内容的行打印出来。

其**命令格式**如下：

```
grep [options] PATTERN [FILE...]
grep [options] [-e PATTERN | -f FILE] [FILE...]
```

或者是在之前介绍的命令后面接管道符"|"再接grep正则表达式。

其**常用参数选项**如下：

- `-n`:在输出的每行前面加上它所在的文件中它的行号
- `-v`:改变匹配的意义，只选择不匹配的行。
- `-E`：`grep -E`==egrep，后接正则表达式。将模式 PATTERN 作为一个扩展的正则表达式来解释 

- `-e`:只能传递一个参数

### 1.1 匹配符与重复操作符介绍

#### 1.1.1 匹配符

- `\` :转义字符`\+ \< \>`

- `.` : 匹配任意单个字符 

- `[1234abc]，[^1234]，[1-5]，[a-d]` :字符序列单字符占位 ,所有数字和数字匹配自身

  `[1-5]`匹配12345中的一个

  `[^a-d]`:匹配不是abcd中的任意一个字符

- `^` :行首 ,^.k 

- `$`: 行尾 ,.k$ 

-  `\<,\>` :单词首尾边界 ;`\<abc`表示以abc开头的，`abc\>`表示以abc结尾的，`\<are\>`表示are单词

-  `| `:连接操作符，并集 ；（\<are\>)| (\<you\>) 表示出现are或出现you都符合条件

- `,`： 选择操作符 ，即多种情况时，以逗号分开
- `\n`：反向引用

#### 1.1.2 重复操作符

- `*`:匹配 0 到多次 

- `?`: 匹配 0 到 1 次

- `+`:匹配 1 到多次

-  `{n}`: 匹配 n 次
- `{n,}`：匹配n到多次
- `{m,n}`：匹配m到n次

除了`*`是基本操作符外，其余全部是扩展操作符。grep命令默认工作于基本模式，使用`grep -E`使工作于扩展模式。

**注意**：

基本正则表达式中元字符 ：?，+，{，|，(和)丢失了特殊意义，需要加\，

反斜杠： `\?，\+，\{，\|，\(以及\)`，**扩展模式**不需要加反斜杠 

### 1.2 实例

&emsp;&emsp;下面以文件`hello.txt`为例来展示grep的操作 

```
hello world 
are you ok? 
areyou ok? 
areyou are youok? 
aaare you ok? 
aare you ok 
aaaare you ok 
abcre you ok? 
xxre you ok 
are yyyou ok? 
xk
zk
ok
yk
zzk 
zxzxk 
bxx 
cxx 
dxx 
areyou are youok? 
zk kz 1 
kz zk 2 
okk koo 3 
zkkz 
kzzk
```

1. 匹配 are aare aaare，0 到多个 a 字符

   ```
   [root@node1 ~]# grep "a*re" hello.txt 
   ```

   ![image-20211210192547060](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112101925514.png)

2. 匹配“a 任意字符 re”

   ```
   [root@node1 ~]# grep "a.re" hello.txt 
   ```

   ![image-20211210192714083](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112101927908.png)

3. 匹配 a 一个到多个任意字符 re，此时，要使用扩展模式或者`\+`

   ```
   [root@node1 ~]# grep -E "a+re" hello.txt 
   或者
   [root@node1 ~]# grep "a\+re" hello.txt 
   ```

   ![image-20211210193651374](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112101936294.png)

4. 匹配包含 bcd 中任意一个字符的行

   ```
   [root@node1 ~]# grep "[b-d]" hello.txt 
   ```

   ![image-20211210195121320](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112101951646.png)

5. 基本正则模式，查找带问号的行,所以需要在基本模式下

   ```
   [root@node1 ~]# grep "?" hello.txt
   ```

   ![image-20211210195247150](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112101952900.png)

6. 匹配“a 任意一个字符或二个字符 re”,`.`表示任意一个字符，依此类推

   ```
   [root@node1 ~]# grep "a.re" hello.txt 
   
   [root@node1 ~]# grep "a..re" hello.txt 
   ```

   ![image-20211210195639720](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112101956966.png)

7. 匹配 zk 和 xk

   ```
   [root@node1 ~]# grep "[xz]k" hello.txt
   ```

   ![image-20211210195844763](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112101958990.png)

8. 匹配不是 zk 和 xk 的

   ```
   [root@node1 ~]# grep "\<[^zx]k" hello.txt
   ```

   ![image-20211210200059668](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112102001798.png)

9. 匹配行首，该行第二个字符一定得是 k

   ```
   [root@node1 ~]# grep "^.k" hello.txt 
   ```

   ![image-20211210200328499](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112102003909.png)

10. 匹配行尾，该行最少两个字符，最后一个是 k

    ```
    [root@node1 ~]# grep ".k$" hello.txt 
    ```

    ![image-20211210200609799](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112102006891.png)

11. 匹配单词边界

    ```
    [root@node1 ~]# grep "\<are\>" hello.txt
    ```

    ![image-20211210200856409](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112102008716.png)

12. 匹配单词开头

    ```
    [root@node1 ~]# grep "\<are" hello.txt
    ```

    ![image-20211210200950063](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112102009218.png)

13. 匹配单词尾

    ```
    [root@node1 ~]# grep "re\>" hello.txt
    ```

    ![image-20211210201052738](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112102010903.png)

14. 连接操作，取并集

    ```
    [root@node1 ~]# grep -E "(\<a*re\>)|(\<you\>)" hello.txt
    ```

    ![image-20211210201300596](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112102013807.png)

15. 匹配该行中 3 个 a 重复的 

    ```
    [root@node1 ~]# grep -E "a{3}" hello.txt
    ```

    ![image-20211210201406989](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112102014581.png)

## 2. Shell脚本编程

### 2.1 Shell脚本编程概述

&emsp;&emsp;比如，现在有5000台机器构成的集群，有一个课件，想向每台机器上都上传一份该课件,如何做？

答：写一个**shell脚本**，将5000台机器的ip地址写到列表里面，利用for循环，循环遍历ip地址，在循环内部将课件传输到每台机器上。

&emsp;&emsp;shell就是一个**bash程序**（进程），启动bash/python进程解释在脚本里编写的命令

- 解释器，启动器：
  - 用户交互输入
  - 文本文件输入

**脚本本质**：

- `#!/bin/bash`（大多数的脚本都是bash脚本）
- `#!/usr/bin/python`

**读取（运行）脚本方式**：`bash/sh file`

- 当前shell：`source file`
- 新建子shell：`/bin/bash file`或者`./file.sh`（需要`chmod a+x  file.sh`，给脚本**添加执行权限**，一般直接创建的文件是没有执行权限的，`./file.sh`需要该文件的执行权限）

**shell脚本的好处**：命令行中的命令都可以放到一个文件中，省的每次都得重新写大量的shell命令。

**补充**：

- `echo $$`打印当前bash的PID

  ![](04 shellscript脚本编程/202112102144300-16391439406832.png)

- `pstree -p`打印当前进程树

  ```
  [root@node1 ~]# pstree -p
  init(1)─┬─auditd(924)───{auditd}(925)
          ├─crond(1284)
          ├─mingetty(1310)
          ├─mingetty(1312)
          ├─mingetty(1314)
          ├─mingetty(1316)
          ├─mingetty(1319)
          ├─mingetty(1321)
          ├─mysqld_safe(1029)───mysqld(1235)─┬─{mysqld}(1236)
          │                                  ├─{mysqld}(1237)
          │                                  ├─{mysqld}(1238)
          │                                  ├─{mysqld}(1239)
          │                                  ├─{mysqld}(1240)
          │                                  ├─{mysqld}(1241)
          │                                  ├─{mysqld}(1242)
          │                                  ├─{mysqld}(1243)
          │                                  ├─{mysqld}(1244)
          │                                  ├─{mysqld}(1245)
          │                                  ├─{mysqld}(1246)
          │                                  ├─{mysqld}(1247)
          │                                  ├─{mysqld}(1249)
          │                                  ├─{mysqld}(1250)
          │                                  ├─{mysqld}(1251)
          │                                  ├─{mysqld}(1252)
          │                                  ├─{mysqld}(1253)
          │                                  ├─{mysqld}(1254)
          │                                  ├─{mysqld}(1255)
          │                                  ├─{mysqld}(1256)
          │                                  ├─{mysqld}(1257)
          │                                  ├─{mysqld}(1258)
          │                                  ├─{mysqld}(1259)
          │                                  ├─{mysqld}(1260)
          │                                  ├─{mysqld}(1261)
          │                                  ├─{mysqld}(1262)
          │                                  └─{mysqld}(1266)
          ├─ntpd(998)
          ├─rsyslogd(940)─┬─{rsyslogd}(941)
          │               ├─{rsyslogd}(942)
          │               └─{rsyslogd}(943)
          ├─sshd(990)───sshd(1323)───bash(1325)───pstree(1340)
          └─udevd(373)─┬─udevd(660)
                       └─udevd(1318)
  [root@node1 ~]# nohup tail -F install.log &
  [1] 1341
  [root@node1 ~]# nohup: 忽略输入并把输出追加到"nohup.out"
  
  [root@node1 ~]# pstree -p
  init(1)─┬─auditd(924)───{auditd}(925)
          ├─crond(1284)
          ├─mingetty(1310)
          ├─mingetty(1312)
          ├─mingetty(1314)
          ├─mingetty(1316)
          ├─mingetty(1319)
          ├─mingetty(1321)
          ├─mysqld_safe(1029)───mysqld(1235)─┬─{mysqld}(1236)
          │                                  ├─{mysqld}(1237)
          │                                  ├─{mysqld}(1238)
          │                                  ├─{mysqld}(1239)
          │                                  ├─{mysqld}(1240)
          │                                  ├─{mysqld}(1241)
          │                                  ├─{mysqld}(1242)
          │                                  ├─{mysqld}(1243)
          │                                  ├─{mysqld}(1244)
          │                                  ├─{mysqld}(1245)
          │                                  ├─{mysqld}(1246)
          │                                  ├─{mysqld}(1247)
          │                                  ├─{mysqld}(1249)
          │                                  ├─{mysqld}(1250)
          │                                  ├─{mysqld}(1251)
          │                                  ├─{mysqld}(1252)
          │                                  ├─{mysqld}(1253)
          │                                  ├─{mysqld}(1254)
          │                                  ├─{mysqld}(1255)
          │                                  ├─{mysqld}(1256)
          │                                  ├─{mysqld}(1257)
          │                                  ├─{mysqld}(1258)
          │                                  ├─{mysqld}(1259)
          │                                  ├─{mysqld}(1260)
          │                                  ├─{mysqld}(1261)
          │                                  ├─{mysqld}(1262)
          │                                  └─{mysqld}(1266)
          ├─ntpd(998)
          ├─rsyslogd(940)─┬─{rsyslogd}(941)
          │               ├─{rsyslogd}(942)
          │               └─{rsyslogd}(943)
          ├─sshd(990)───sshd(1323)───bash(1325)─┬─pstree(1342)
          │                                     └─tail(1341)
          └─udevd(373)─┬─udevd(660)
                       └─udevd(1318)
  [root@node1 ~]# echo $$
  1325
  ```

- 一个简单的shell脚本例子

  ```
  [root@node1 ~]# vim /opt/mysh.sh
  
  #!/bin/bash 
  ls -l / 
  echo "hello world!" 
  echo $$ 
  pstree
  ```

​	![image-20211210220911486](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112102209766.png)

### 2.2 awk脚本编程实现消费统计报表

案例报表统计：合计每人 1 月总消费，0：manager，1：worker

```
Tom 0 2020-10-11 car 3000 
John 1 2020-01-13 bike 1000 
vivi 1 2020-01-18 car 2800 
Tom 0 2020-01-20 car 2500 
John 1 2020-01-28 bike 3500
```

​		新建emp.txt文件，将上述内容写入文件中，

1.查看文件的分隔符，awk默认制表符与分隔符
2.日期是第三列，由于统计到的是一月份，所以需要拆分，以"-"进行拆分，并存放到date变量中
3.for循环进行输出，写法与C语言的写法一致

```
[root@node1 ~]# vim awk.sh

#!/bin/awk -f
{ 
 split($3, date, "-");  
 if (date[2] == "01") {    
   name[$1]+=$5;   
     if($2=="0"){     
        role[$1]="Manager"    
     }else{
        role[$1]="Worker" 
   } 
 }
}
END{
 for(i in name){ 
  print i"\t"name[i]"\t"role[i] 
  } 
}


[root@node1 ~]# chmod +x awk.sh
[root@node1 ~]# ./awk.sh emp.txt 
```

![image-20211211130038782](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112111300166.png)

### 2.3 shell脚本中函数的定义与调用

**定义**：

 ```
 java 语言： 
 public int methodName(int x,String name){ 
 ......
 }
 
 javascript: 
 function mN(x,name){
 ...... 
 }
 
 Shell脚本语言： 
 myshellname () { 
 	command1 
 	command2 
 	command3 
 	…… 
 }
 ```

**调用**：直接调用函数名（myshellname）即可
**案例**：在shell脚本中展示函数的定义与调用

```
[root@node1 ~]# vim fun.sh

#!/bin/bash
myFunName(){
   echo "hello function,args:$1 $2"
}
myFunName url geekmooc.cn


[root@node1 ~]# chmod +x fun.sh 
[root@node1 ~]# ll fun.sh 
-rwxr-xr-x 1 root root 89 12月 11 13:12 fun.sh
[root@node1 ~]# ./fun.sh 
hello function,args:url geekmooc.cn
```

**总结来说**:

- bash是一个程序，shell是一个bash进程。

- bash是一个解释器、启动器，解释执行用户的输入命令，可以通过shell启动其他进程。

- 将要执行的命令放到一个文件中，在文件开头：

  ```
  #!/bin/bash 
  #!/usr/bin/python 
  #!/bin/awk -f
  ```

​		用于指定该脚本由哪个程序负责解释执行

- 定义函数与调用函数

  ```
  定义函数：
  funName() { 
     各种命令 
  }
  调用函数：直接输入 funName与需要的参数即可
  ```

### 2.4 Shell脚本中的变量的使用

&emsp;&emsp;变量有以下5种类型：

- 环境变量

  ![image-20211211144038181](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112111440530.png)

- 本地变量

- 局部变量： 

- 位置变量

- 特殊变量

#### 2.4.1 本地变量

- 当前shell脚本所有

- 生命周期跟当前shell一样

  ```
  [root@node1 ~]# vim var.sh
  
  #!/bin/bash
  a=99 # 本地变量 
  echo $a 
  myfunc() {
    myvar=99
    echo $myvar 
  }
  echo "before-myfunc-$myvar" #访问不到 
  myfunc #调用函数 
  echo "after-myfunc-$myvar" #可以访问到
  
  [root@node1 ~]# chmod +x var.sh 
  [root@node1 ~]# ./var.sh 
  99
  before-myfunc-
  99
  after-myfunc-99
  ```

#### 2.4.2 局部变量

- 只在函数内部有效，即只有调用函数才可以访问变量

- 需要在变量前面加local

  ```
  [root@node1 ~]# vim var.sh 
  
  #!/bin/bash
  a=99 # 本地变量 
  echo $a 
  myfunc() {
    local myvar=99 #局部变量 
    echo "in func $myvar" 
  }
  echo "before-myfunc-$myvar" #访问不到 
  myfunc #调用函数 
  echo "after-myfunc-$myvar" #仍然访问不到
  
  [root@node1 ~]# chmod +x var.sh 
  [root@node1 ~]# ./var.sh 
  99
  before-myfunc-
  in func 99
  after-myfunc-
  ```

#### 2.4.3 位置变量

&emsp;&emsp;在shell脚本中，位置变量用于接收参数

- `$1,$2,${11}`：需要注意当大于等于10时，需要`echo ${10}`

  ```
  [root@node1 ~]# vim myvar1.sh 
  
  #!/bin/bash
  myfunc1(){
    echo ${10}
  }
  
  myfunc1 1 2 3 4 5 6 7 8 9 10 11  # 调用函数
  
  [root@node1 ~]# chmod +x myvar1.sh 
  [root@node1 ~]# ./myvar1.sh 
  10
  ```

#### 2.4.4 特殊变量

- `$#`：表示位置参数的个数

  ```
  [root@node1 ~]# fun(){
  > echo $#
  > }
  [root@node1 ~]# fun 1 2 3 4
  4
  [root@node1 ~]# fun 1 2 3 4 a b c d
  8
  ```

- `$*`：参数列表，双引号引用为一个字符串，所有的参数作为一个字符串

  ```
  [root@node1 ~]# fun(){
  > echo $* 
  > }
  [root@node1 ~]# fun 1 2 3 4
  1 2 3 4
  [root@node1 ~]# fun 1 2 3 4 a b c d
  1 2 3 4 a b c d
  ```

- `$@`：参数列表，双引号引用为单独的字符串，所有的参数作为单个字符串

  ```
  [root@node1 ~]# fun(){ echo $@; }
  [root@node1 ~]# fun 1 2 3 4
  1 2 3 4
  [root@node1 ~]# fun 1 2 3 4 a b c d
  1 2 3 4 a b c d
  
  ```

- `$$`：当前shell的PID：接收者

  - `$BASHPID`：真实值

  ```
  [root@node1 ~]# echo $$
  1330
  [root@node1 ~]# echo $BASHPID
  1330
  [root@node1 ~]# echo "hello"|echo $$
  1330
  [root@node1 ~]# echo "hello"|echo $BASHPID
  1577
  ```

- `$?`：上一个命令的退出状态

  - 0：成功
  - 其他：失败

  ```
  [root@node1 ~]# ls /est
  ls: 无法访问/est: 没有那个文件或目录
  [root@node1 ~]# echo $?
  2
  [root@node1 ~]# ls /
  bin   dev      etc   lib    lost+found  mnt  proc  sbin     srv  tmp  var
  boot  dirtest  home  lib64  media       opt  root  selinux  sys  usr
  [root@node1 ~]# echo $?
  0
  ```

### 2.5 Shell脚本中数组的使用

&emsp;&emsp;数组**定义**如下：

数组赋值可以使用复合赋值的方式，形式是 `name=(value1 ... valuen)`，这里每个 value 的形式都是[subscript]=string。string 必须出现。如果出现了可选的括号和下标，将为这个下标赋值，否则被赋值的元素的下标是语句中上一次赋值的下标加一。下标从 0 开始。这个语法也被内建命令declare 所接受。单独的数组元素可以用上面介绍的语法 name[subscript]=value 来赋值。

- 数组的任何元素都可以用 `${name[subscript]}` 来引用。花括号是必须的，以避免和路径扩展冲突。 
- 内建命令 unset 用于销毁数组。`unset name[subscript]` 将销毁下标是 subscript 的元素 。unset name, 这里 name 是一个数组，或者 unset name[subscript], 这里 subscript 是 * 或者是 @，将销毁整个数组。

```
[root@node1 ~]# sxt=(a b c)
[root@node1 ~]# echo ${sxt}
a
[root@node1 ~]# echo ${sxt[0]}
a
[root@node1 ~]# echo ${sxt[1]}
b
[root@node1 ~]# echo ${sxt[2]}
c
[root@node1 ~]# sxt[3]=d
[root@node1 ~]# echo ${sxt[3]}
d
[root@node1 ~]# echo ${sxt[*]}
a b c d
[root@node1 ~]# echo ${sxt[@]}
a b c d
[root@node1 ~]# unset sxt[3]
[root@node1 ~]# echo ${sxt[@]}
a b c
```

### 2.6 横跨父子进程的变量

&emsp;&emsp;横跨父子进程的变量是特殊使用场景下的变量。**父进程修改值不会影响子进程的变化，子进程值的修改也不会影响父进程的值。**

在子进程中定义了变量b=22，在父进程中访问b访问不到。

```
[root@bk1 ~]# a=9 
[root@bk1 ~]# echo $a 
9
[root@bk1 ~]# b=22 | echo ok 
ok
[root@bk1 ~]# echo $b #访问不到子进程的数据 

[root@bk1 ~]#
```

在父进程中定义了变量a=9，在子进程中访问a访问不到。

```
[root@node1 ~]# vim fs.sh

#!/bin/bash
echo $a
echo "---------------------------------"
pstree -p


[root@node1 ~]# chmod +x fs.sh 
[root@node1 ~]# ./fs.sh 

---------------------------------
init(1)─┬─auditd(922)───{auditd}(923)
        ├─crond(1289)
        ├─mingetty(1315)
        ├─mingetty(1317)
        ├─mingetty(1320)
        ├─mingetty(1322)
        ├─mingetty(1324)
        ├─mingetty(1326)
        ├─mysqld_safe(1027)───mysqld(1233)─┬─{mysqld}(1234)
        │                                  ├─{mysqld}(1235)
        │                                  ├─{mysqld}(1236)
        │                                  ├─{mysqld}(1237)
        │                                  ├─{mysqld}(1238)
        │                                  ├─{mysqld}(1239)
        │                                  ├─{mysqld}(1240)
        │                                  ├─{mysqld}(1241)
        │                                  ├─{mysqld}(1242)
        │                                  ├─{mysqld}(1243)
        │                                  ├─{mysqld}(1244)
        │                                  ├─{mysqld}(1245)
        │                                  ├─{mysqld}(1247)
        │                                  ├─{mysqld}(1248)
        │                                  ├─{mysqld}(1249)
        │                                  ├─{mysqld}(1250)
        │                                  ├─{mysqld}(1251)
        │                                  ├─{mysqld}(1252)
        │                                  ├─{mysqld}(1253)
        │                                  ├─{mysqld}(1254)
        │                                  ├─{mysqld}(1260)
        │                                  ├─{mysqld}(1261)
        │                                  ├─{mysqld}(1263)
        │                                  ├─{mysqld}(1264)
        │                                  ├─{mysqld}(1265)
        │                                  ├─{mysqld}(1266)
        │                                  └─{mysqld}(1271)
        ├─ntpd(996)
        ├─rsyslogd(938)─┬─{rsyslogd}(939)
        │               ├─{rsyslogd}(940)
        │               └─{rsyslogd}(941)
        ├─sshd(988)───sshd(1328)───bash(1330)───fs.sh(1599)───pstree(1600)
        └─udevd(373)─┬─udevd(660)
                     └─udevd(1319)
```

但是，如果将a导出为环境变量，则子进程就可以访问a了

```
[root@node1 ~]# export a
[root@node1 ~]# ./fs.sh 
9
---------------------------------
```

并且，如果子进程睡 20s，在此期间修改环境变量的值，子进程还是打印之前的值。

### 2.7 单引号、双引号、花括号与反引号

- 单引号：强引用，不可嵌套

- 双引号：弱引用，参数扩展

  ```
  [root@node1 ~]# x=89
  [root@node1 ~]# echo "x=$x"
  x=89
  [root@node1 ~]# echo 'x=$x'
  x=$x
  [root@node1 ~]# echo "\"$x"\"
  "89"
  ```

- 花括号扩展不能被引用

  花括号扩展，创建 adir，bdir，cdir 三个目录

  ```
  [root@node1 ~]# mkdir ./{a,b,c}dir
  [root@node1 ~]# ls
  adir             cdir          etc02.tar.gz  inittab             ln2        ok1.log  profile_path.txt
  anaconda-ks.cfg  dirtest       fs.sh         install.log         myvar1.sh  ok2.log  sed.txt
  awk.sh           emp.txt       fun.sh        install.log.syslog  new.log    passwd   sort.txt
  bdir             etc01.tar.gz  hello.txt     ln1                 nohup.out  profile  var.sh
  ```

  花括号扩展，拷贝/etc/profile 以及/etc/init.d/network 到当前目录

  ```
  [root@node1 adir]# cd ../bdir
  [root@node1 bdir]# cp /etc/{profile,init.d/network} ./
  [root@node1 bdir]# ll
  总用量 12
  -rwxr-xr-x 1 root root 6334 12月 11 17:28 network
  -rw-r--r-- 1 root root 1870 12月 11 17:28 profile
  ```

- 反引号`:实现命令替换

  命令替换允许我们**将 shell 命令的输出赋值给变量**。它是脚本编程中的一个主要部分。 命令替换会创建子 shell 进程来运行相应的命令。子 shell 是由运行该脚本的 shell 所创建出来的一个独立的子进程。由该子进程执行的命令无法使用脚本中所创建的变量。

  ```
  [root@node1 apps]# myvar=`echo "hello"`
  [root@node1 apps]# echo $myvar
  hello
  ```

  **实例**：开启虚拟机node2，从虚拟机node1上拷贝一个文件发到node2上

  ```
  [root@node1 bdir]# cd /opt/apps/
  [root@node1 apps]# ls
  jdk-7u80-linux-x64.rpm  nginx-1.8.1  nginx-1.8.1.tar.gz  rpm_install_mysql
  [root@node1 apps]# scp nginx-1.8.1.tar.gz node2:/opt/apps
  The authenticity of host 'node2 (192.168.236.102)' can't be established.
  RSA key fingerprint is 8e:0e:3f:82:c9:aa:4f:bd:4d:b0:b3:9c:82:a7:b9:86.
  Are you sure you want to continue connecting (yes/no)? yes
  Warning: Permanently added 'node2,192.168.236.102' (RSA) to the list of known hosts.
  root@node2's password: 
  nginx-1.8.1.tar.gz                                                   100%  814KB 813.9KB/s   00:00    
  [root@node1 apps]# scp nginx-1.8.1 node2:`pwd`
  root@node2's password: 
  nginx-1.8.1: not a regular file
  [root@node1 apps]# scp jdk-7u80-linux-x64.rpm node2:`pwd`
  root@node2's password: 
  jdk-7u80-linux-x64.rpm                                               100%  132MB 131.7MB/s   00:01  
  ```

  ```
  [root@node1 apps]# scp nginx-1.8.1 node2:`pwd`
  ```

  pwd反引号表示将命令的输出作为变量

​		反引号提升扩展优先级，先执行反引号的内容，再执行其他的。 

​		错误：`myvar=echo "hello" `

​		正确： myvar=\`echo "hello"\` 

- 命令替换的嵌套:

  ```
  myvar=$(echo $(echo "hello world")) 
  
  myvar=$(echo "hello world") 
  
  myvar="hello world"
  ```


### 2.8 表达式

#### 2.8.1 逻辑表达式

- command1 && command2（与）

  如果command1的退出状态是0，则执行command2（只有第一个成立才会走第二个）

  ```
  [root@node1 apps]# test -d /bin && echo "文件夹/bin存在"
  文件夹/bin存在
  或者
  [root@node1 apps]# [ -d /bin ] && echo "文件夹/bin存在"
  文件夹/bin存在
  ```

- command1 || command2 （或）

  如果 command1 的退出状态不是 0，则执行 command2 （只有第一个不成立才会走第二个）

  ```
  [root@node1 apps]# test -d /hello || echo "文件夹/hello不存在" 
  文件夹/hello不存在
  ```

#### 2.8.2 算术表达式

&emsp;&emsp;算术表达书即变量的四则运算，以加号为例，

- let算术运算表达式

  其基本格式为：`let C=$A+$B`

- $[算术表达式]

  其基本格式为：`C=$[$A+$B]`

- $((算术表达式))

  其基本命令格式为：`C=$((A+B))`

- expr 算术表达式

  其基本命令格式为：C=\`expr $A + $B\`，表达式中各操作数及运算符之间要有空格，同时要使用命令引用

  ```
  [root@node1 apps]# a=1
  [root@node1 apps]# b=2
  [root@node1 apps]# let c=$a+$b
  [root@node1 apps]# echo $c
  3
  [root@node1 apps]# c1=$[$a+$b]
  [root@node1 apps]# echo $c1
  3
  [root@node1 apps]# c2=$((a+b))
  [root@node1 apps]# echo $c2
  3
  [root@node1 apps]# c3=`expr $a + $b`
  [root@node1 apps]# echo $c3
  3
  ```

  **前置**++表示先自身++之后再参与计算

  **后置**++表示先计算，再自身++

  ```
  [root@node1 apps]# d=$((a++-b))
  [root@node1 apps]# echo $d
  -1
  [root@node1 apps]# echo $a
  2
  [root@node1 apps]# a=1
  [root@node1 apps]# b=2
  [root@node1 apps]# d=$((a-++b))
  [root@node1 apps]# echo $d
  -2
  [root@node1 apps]# echo $a
  1
  [root@node1 apps]# echo $b
  3
  ```

#### 2.8.3 条件表达式

&emsp;&emsp;条件表达式的形式有以下三种：

- `[ 表达式 ]`
- `test 表达式`
- `[[ 表达式 ]]`

test的参数选项：

```
-a FILE        True if file exists.
-b FILE        True if file is block special.
-c FILE        True if file is character special.
-d FILE        True if file is a directory.
-e FILE        True if file exists.
-f FILE        True if file exists and is a regular file.
-g FILE        True if file is set-group-id.
-h FILE        True if file is a symbolic link.
-L FILE        True if file is a symbolic link.
-k FILE        True if file has its `sticky' bit set.
-p FILE        True if file is a named pipe.
-r FILE        True if file is readable by you.
-s FILE        True if file exists and is not empty.
-S FILE        True if file is a socket.
-t FD          True if FD is opened on a terminal.
-u FILE        True if the file is set-user-id.
-w FILE        True if the file is writable by you.
-x FILE        True if the file is executable by you.
-O FILE        True if the file is effectively owned by you.
-G FILE        True if the file is effectively owned by your group.
-N FILE        True if the file has been modified since it was last read.
```

```
[root@node1 ~]# test -r sed.txt && echo "have r"
have r
[root@node1 ~]# test -w sed.txt && echo "have w"
have w
[root@node1 ~]# test -x sed.txt && echo "have x"
[root@node1 ~]# test 3 -gt 2 && echo "ok"
ok
[root@node1 ~]# test 3 -gt 4 && echo "ok"
```

### 2.9 分支

- if
- case

#### 2.9.1 if分支

- 单分支结构

  ```
  if [ 条件判断 ] 
   then
     //命令 
  fi
  
  或者
  
  if [ 条件判断 ]; then 
   条件成立执行，命令; 
  ```

  fi :将 if 反过来写,就成为 fi ，用于结束 if 语句

  **实例**：

  ```
  if [ 3 -gt 2 ];then
    echo ok
  fi
  
  或者
  if [ 3 -gt 8 ]
   then echo ok;
  fi
  ```

- 双分支结构

  ```
  if [ 条件 1 ];then 
    条件 1 成立执行，指令集 1 
  else
    条件 1 不成执行指令集 2; 
  fi
  ```

- 多分支结构

  ```
  if [ 条件 1 ];then 
    条件 1 成立，执行指令集 1 
  elif [ 条件 2 ];then 
    条件 2 成立，执行指令集 2 
  else
    条件都不成立，执行指令集 3 
  fi
  ```

  **实例**：猜数字游戏

  ```
  [root@node1 ~]# vim sh01.sh
  
  #!/bin/bash 
  a=20 
  if [ $a -gt $1 ]; then
    echo "你输入的数字太小" elif [ $a -eq $1 ]; then
    echo "恭喜哈，数字相等" 
  else
    echo "你输入的数字太大" 
  fi
  
  [root@node1 ~]# chmod +x sh01.sh 
  [root@node1 ~]# ./sh01.sh 30
  你输入的数字太大
  ```

#### 2.9.2 case分支

```
case $变量名称 in 
"值1")
  程序段1
;;
"值2")
  程序段2
;;
*)
  exit 1
;;

esac
```

**实例**：判 断 用 户 输 入 的 是 哪 个 数 ， 1-7 显 示 输 入 的 数 字 ， 1 显 示 Mon,2 :Tue,3:Wed,4:Thu,5:Fir,6-7:weekend,其它值的时候，提示：please input [1,7]， 该如何实现？ 

```
[root@node1 ~]# vim num2wk.sh

#!/bin/bash
read -p "please input a number[1,7]:" num 
case $num in 
1)   
  echo "Mon" 
;;
2)   
  echo "Tue" 
;;
3)   
  echo "Wed" 
;;
4) 
  echo "Thu" 
;;
5) 
  echo "Fir" 
;;
[6-7]) 
  echo "weekend" 
;;
*) 
  echo "please input [1,7]" 
;;

esac

[root@node1 ~]# chmod +x num2wk.sh 
[root@node1 ~]# ./num2wk.sh 
please input a number[1,7]:1
Mon
[root@node1 ~]# ./num2wk.sh 
please input a number[1,7]:2
Tue
[root@node1 ~]# ./num2wk.sh 
please input a number[1,7]:3
Wed
[root@node1 ~]# ./num2wk.sh 
please input a number[1,7]:4
Thu
[root@node1 ~]# ./num2wk.sh 
please input a number[1,7]:5
Fir
[root@node1 ~]# ./num2wk.sh 
please input a number[1,7]:6
weekend
[root@node1 ~]# ./num2wk.sh 
please input a number[1,7]:7
weekend
[root@node1 ~]# ./num2wk.sh
please input a number[1,7]:8
please input [1,7]
```

### 2.10 循环

- while
- for

#### 2.10.1 while循环

- while循环语句格式

  ```
  while [ condition ];do
    命令
  done
  
  或者
  
  while [ condition ];
  do
    命令
  done
  ```

- 案例1：每隔两秒打印系统负载情况，如何实现？

  ```
  [root@node1 ~]# vim while1.sh
  
  #!/bin/bash 
  while true 
   do
    uptime #系统负载情况 
    sleep 2 #休眠 2 秒 
  done
  
  [root@node1 ~]# chmod +x while1.sh 
  [root@node1 ~]# ./while1.sh
   21:21:11 up  9:12,  1 user,  load average: 0.00, 0.00, 0.00
   21:21:13 up  9:12,  1 user,  load average: 0.00, 0.00, 0.00
   21:21:15 up  9:12,  1 user,  load average: 0.00, 0.00, 0.00
  ^C
  ```

- 案例2：使用 while 循环，编写 shell 脚本，计算 1+2+3+…+100 的和并输出，如何实现？

  ```
  [root@node1 ~]# vim while2.sh
  
  #!/bin/bash
  sum=0
  i=1
  while [ $i -le 100 ]
  do
    sum=$((sum+i))
    i=$((i+1))
  done
  
  echo "1+2+3+...+100=$sum"
  
  [root@node1 ~]# chmod +x while2.sh 
  [root@node1 ~]# ./while2.sh 
  1+2+3+...+100=5050
  ```

  或者

  ```
  [root@node1 ~]# vim while3.sh
  
  #!/bin/bash
  sum=0
  i=1
  while [ $i -le 100 ]
  do
    sum=$((sum+i++))
  done
  
  echo "1+2+3+...+100=$sum"
  
  [root@node1 ~]# chmod +x while3.sh 
  [root@node1 ~]# ./while3.sh 
  1+2+3+...+100=5050
  ```

#### 2.10.2 for循环

- for循环语法格式

  ```
  for 变量名 in 变量取值列表
  do
    命令
  done
  ```

  提示：在此结构中“in 变量取值列表”可省略，省略时相当于使用 for i in “$@”

  **实例**：for循环可以配合使用大括号的方法

  ```
  [root@node1 ~]# vim for1.sh
  
  #!/bin/sh 
  for num in {1..4} 
  do
    echo $num 
  done
  
  [root@node1 ~]# chmod +x for1.sh 
  [root@node1 ~]# ./for1.sh 
  1
  2
  3
  4
  
  [root@bk1 ~]# echo 10.13.20.{1..3} 
  10.13.20.1 10.13.20.2 10.13.20.3
  ```

  使用 seq –s 分隔符 ；起始 步长 终点 seq -s “ ” 2 2 100

  ```
  [root@bk1 ~]# vim for1.sh 
  
  #!/bin/sh 
  for num in `seq -s " " 1 1 5` 
  do
    echo $num 
  done
  ```

#### 2.10.3 查找指定目录下最大的文件

&emsp;&emsp;根据用户给定的目录，输出该目录（包含其子级目录）下文件大小最大的文件。需要考虑递归子目录。 

提示：\$IFS 默认是空格，制表符和换行符，IFS=\$'\n' 使用$获取\n 换行符的 ascii 码作为 IFS 的值，此时分隔符就是换行符了。 

- 步骤：使用 du 命令加-a 遍历用户指定目录（$1 获取）的所有文件，使用管道将结果传递 

  给 sort，让 sort 使用数值序倒序排序，依次输出各个条目。

  ```
  [root@node1 ~]# vim findMax.sh
  
  #!/bin/bash 
  oldIFS=$IFS  # 保存 旧IFS 变量
  IFS=$'\n'  
  for item in `du -a $1 | sort -nr`; do 
    fileName=`echo $item | awk '{print $2}'` 
    if [ -f $fileName ]; then 
      echo "max file is:$fileName"
      break 
    fi 
  done 
  IFS=$oldIFS #用完后重置 IFS 变量的值
  
  [root@node1 ~]# chmod +x findMax.sh 
  [root@node1 ~]# ./findMax.sh 
  max file is:./etc01.tar.gz
  [root@node1 ~]# ./findMax.sh /etc/
  max file is:/etc/selinux/targeted/policy/policy.24
  ```

### 2.11 bash的七步扩展

1. 花括号 

   ```
   mkdir {a,b,c}dir 
   mkdir adir bdir cdir
   ```

2. 波浪线扩展

   ```
   cd ~god
   cd /home/god
   ```

3. 变量和参数替换

   ```
   $变量名
   $1
   $@
   ```

4. 命令替换

   ```
   ls -l `echo $path`
   ```

5. 算术扩展

   ```
   num=$((x+3+4)) 
   let num =$x+$y+5
   ```

6. 单词拆分

   ```
   $IFS 通过$IFS 对数据进行拆分
   ```

7. 路径*（0到多个任意字符）？（1个字符）

   ```
   [root@node1 ~]# ll *.txt
   -rw-r--r-- 1 root root 139 11月 27 16:41 emp.txt
   -rw-r--r-- 1 root root 241 12月 10 19:01 hello.txt
   -rw-r--r-- 1 root root 144 11月 27 14:26 profile_path.txt
   -rw-r--r-- 1 root root 191 11月 26 20:32 sed.txt
   -rw-r--r-- 1 root root  66 11月 26 18:50 sort.txt
   [root@node1 ~]# ll ?.txt
   ls: 无法访问?.txt: 没有那个文件或目录
   ```

### 2.12 shell script脚本检查

其**命令格式**为：`sh [-nvx] scripts.sh`

**参数选项**：

- `-n`：不执行script，仅查询语法问题

- `-v`：在执行 script 前,先将 scripts 的内容输出到屏幕上;

- `-x`：将使用到的 script 内容显示到屏幕上,这是很有用的参数;

  ```
  [root@node1 ~]# sh -x myvar1.sh 
  + myfunc1 1 2 3 4 5 6 7 8 9 10 11
  + echo 10
  10
  
  [root@node1 ~]# sh -x findMax.sh /etc/
  + oldIFS=' 	
  '
  + IFS='
  '
  ++ sort -nr
  ++ du -a /etc/
  + for item in '`du -a $1 | sort -nr`'
  ++ echo '25456	/etc/'
  ++ awk '{print $2}'
  + fileName=/etc/
  + '[' -f /etc/ ']'
  + for item in '`du -a $1 | sort -nr`'
  ++ echo '19116	/etc/selinux'
  ++ awk '{print $2}'
  + fileName=/etc/selinux
  + '[' -f /etc/selinux ']'
  + for item in '`du -a $1 | sort -nr`'
  ++ echo '19096	/etc/selinux/targeted'
  ++ awk '{print $2}'
  + fileName=/etc/selinux/targeted
  + '[' -f /etc/selinux/targeted ']'
  + for item in '`du -a $1 | sort -nr`'
  ++ echo '11584	/etc/selinux/targeted/modules'
  ++ awk '{print $2}'
  + fileName=/etc/selinux/targeted/modules
  + '[' -f /etc/selinux/targeted/modules ']'
  + for item in '`du -a $1 | sort -nr`'
  ++ awk '{print $2}'
  ++ echo '11580	/etc/selinux/targeted/modules/active'
  + fileName=/etc/selinux/targeted/modules/active
  + '[' -f /etc/selinux/targeted/modules/active ']'
  + for item in '`du -a $1 | sort -nr`'
  ++ awk '{print $2}'
  ++ echo '7128	/etc/selinux/targeted/policy'
  + fileName=/etc/selinux/targeted/policy
  + '[' -f /etc/selinux/targeted/policy ']'
  + for item in '`du -a $1 | sort -nr`'
  ++ awk '{print $2}'
  ++ echo '7124	/etc/selinux/targeted/policy/policy.24'
  + fileName=/etc/selinux/targeted/policy/policy.24
  + '[' -f /etc/selinux/targeted/policy/policy.24 ']'
  + echo 'max file is:/etc/selinux/targeted/policy/policy.24'
  max file is:/etc/selinux/targeted/policy/policy.24
  + break
  + IFS=' 	
  '
  ```

### 2.13 Readfile

#### 2.13.1 问题

&emsp;&emsp;循环遍历文件每一行，通过流程控制语句实现如下功能：

- 定义一个计数器
- 打印 num 正好是文件行数

#### 2.13.2 参考答案

**思路**：定义一个计数器，使用 for 循环从文件内容按行获取元素，每个元素是一行，在 for 循环中递增计数器用于计行数，最后输出总行数。

```
#!/bin/bash 
num=0 
oldIFS=$IFS 
IFS=$'\n' 

for item in `cat $1`; do 
  echo $item 
  ((num++)) 
done 
echo "line number is :$num" 
IFS=$oldIFS
```

