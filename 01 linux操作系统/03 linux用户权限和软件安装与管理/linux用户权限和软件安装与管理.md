[TOC]

# linux用户权限和软件安装与管理

## 1. 用户、用户组、其他人概述

&emsp;&emsp;下面介绍用户、用户组、其他人的概念

![1638084178412](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/1638084178412.png)

&emsp;&emsp;有个人叫张小猪，他是张小猪家的人，和王大毛家没有关系，此时无法进入王大毛家；如果张小猪和王三毛成了好朋友，他就可以通过王三毛进入王家。那么这个张小猪就是王家所谓的其他人（Others） 。

&emsp;&emsp;若某一用户对于一个与他毫无关系的用户组而言,就可以被视作其他用户。 在这有一个特殊用户需要提及一下。Linux 中的 root 用户（上图中的天神）,在整个Linux 系统中拥有最大的权限,理论上能干任何事情。

### 1.1 用户

&emsp;&emsp;由于 Linux 是多用户,多任务的操作系统,为此,会经常有多个用户同时使用某一台主机。为了考虑每个用户的隐私安全以及每个用户特殊的工作环境,设计了文件所有者这个概念。 而文件所有者就是文件所属的用户。

&emsp;&emsp;Linux 系统是一个多用户多任务的分时操作系统，任何一个要使用系统资源的用户，都必须首先向系统管理员申请一个账号，然后以这个账号的身份进入系统。 用户的账号一方面可以帮助系统管理员对使用系统的用户进行跟踪，并控制他们对系统资源的访问；另一方面也可以帮助用户组织文件，并为用户提供安全性保护。每个用户账号都拥有一个唯一的用户名和各自的口令。用户在登录时键入正确的用户名和口令后，就能够进入系统和自己的主目录。

### 1.2 用户组

&emsp;&emsp;用户组是为了团队开发资源而设计的。在 Linux 下我们可以进行简单的文件权限设置,就能限制非自己团队的用户权限,同时,我们还可以设置个人私密文件,使团队内其他成员无法获取私人的文件内容。除此之外,若是有项目主管想监督这两个团队的开发进度,需要查看两个团队文件的权限,你便可以设置主管账号,同时支持这两个用户组。换句话说:每个账号都可以有多个用户组的支持。

&emsp;&emsp;在同一个用户组中，可以通过设置文件的权限，将一些文件设置为“私有的”（只能自己访问和使用），同用户组的其他用户无法访问和使用。 而通过设置用户组共享的，则可让大家共同分享。

```
d rwx r-x ---
```



## 2. 用户和用户组管理

### 2.1 Linux用户身份与用户组记录的文件

&emsp;&emsp;在 Linux 系统当中,默认情况下所有的系统上的账号信息都记录在`/etc/passwd` 这个文件内(包括 root 用户)。而个人密码记录在`/etc/shadow` 这个文件内。所有Linux 的组名都记录在`/etc/group` 内。这三个文件非常重要,不要轻易做变动。用户身份与用户组的概念,能够帮助我们的 Linux 多任务环境变得更为容易管理。

![image-20211205155936699](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211205155936699.png)

​		![image-20211205160038930](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211205160038930.png)

​		![image-20211205160132362](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211205160132362.png)

&emsp;&emsp;实现用户账号的管理，要完成的工作主要有如下几个方面：

- 用户账号的添加、删除与修改
- 用户口令的管理
- 用户组的管理

### 2.2 Linux系统用户账号的管理

&emsp;&emsp;用户账号的管理工作主要涉及到用户账号的添加、修改和删除。 添加用户账号就是在系统中创建一个新账号，然后为新账号分配用户号、用户组、主目录和登录 Shell 等资源。刚添加的账号是被锁定的，无法使用。

1. 添加新的用户账号使用 useradd 命令，其命令格式为：`useradd 选项用户名`

   增加用户账号就是在`/etc/passwd` 文件中为新用户增加一条记录，同时更新其他 系统文件如`/etc/shadow`,` /etc/group` 等。 

   参数介绍：

   - -c ：指定一段注释性描述
   - -d ：指定用户主目录，如果此目录不存在，则同时使用-m 选项，可以创建主目录。不指定的话默认在`/home`目录下创建同名的用户组作为家目录
   - -g ：指定用户所属的用户组
   - -G ：指定用户所属的附加组。
   - -s ：指定用户的登录 Shell
   - -u ：指定用户的用户号，如果同时有-o 选项，则可以重复使用其他用户的标识号。

   实例1：添加用户 lucy,并设置他的个人主目录

   `[root@node1 ~]# useradd –d /usr/lucy -m lucy`

   此命令创建了一个用户 lucy，其中-d 和-m 选项用来为登录名 lucy 产生一个主目录/usr/lucy（/home 为默认的用户主目录所在的父目录）。

   实例2：创建用户 gen，指定他属于主用户组“lucy”，附加组“adm、root”，已经登录 

   的 Shell 是/bin/sh

   `[root@node1 ~]# useradd -s /bin/sh -g lucy -G adm,root gen`

   此命令新建了一个用户 gen，该用户的登录 Shell 是 /bin/sh，它属于lucy 用户组，同时又属于 adm 和 root 用户组，其中 lucy 用户组是其主组。

   ![image-20211205162824510](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211205162824510.png)

 2. 修改账号

    &emsp;&emsp;修改用户账号就是根据实际情况更改用户的有关属性，如用户号、主目录、用户 组、登录 Shell 等。

    &emsp;&emsp;修改已有用户的信息使用 usermod 命令，其格式如下：`usermod 选项用户名` 。常用的选项包括-c, -d, -m, -g, -G, -s, -u 以及-o 等，这些选项的意义与useradd 命令中的选项一样，可以为用户指定新的资源值。另外，有些系统可以使用选项：`-l 新用户名` 这个选项指定一个新的账号，即将原来的用户名改为新的用户名。

    实例1：将用户gen的登录Shell修改为bash，主目录改为/home/z，用户组改为root。

    `usermod -s /bin/bash –g root gen`![image-20211205171423542](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211205171423542.png)

3. 删除帐号

   &emsp;&emsp;如果一个用户的账号不再使用，可以从系统中删除。删除用户账号就是要将`/etc/passwd`等系统文件中的该用户记录删除，必要时还删除用户的主目录。 

   &emsp;&emsp;删除一个已有的用户账号使用 userdel 命令，其格式如下： `userdel 选项用户名` 。常用的选项是`-r`，它的作用是把用户的主目录一起删除。	![image-20211205172308603](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211205172308603.png)

![image-20211205172402321](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211205172402321.png)

​		此 命 令 删 除 用 户 liujie在 系 统 文 件 中 （ 主 要 是 /etc/passwd, /etc/shadow, /etc/group 等）的记录。

​		此时，`/home`家目录下的主目录还有删除，若还要创建该用户，需要先强制删除该主目录再进行创建用户

​		![image-20211205173122348](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211205173122348.png)

4. 用户口令的管理

   &emsp;&emsp;用户管理的一项重要内容是用户口令的管理。用户账号刚创建时没有口令，但是被系统锁定， 无法使用，必须为其指定口令后才可以使用，即使是指定空口令。 指定和修改用户口令的 Shell 命令是 passwd。**超级用户**可以为自己和其他用户指定口令， **普通用户**只能用它修改自己的口令。命令的格式为： `passwd 选项用户名`。

   参数说明：

   - -l（lock） 锁定口令，即禁用账号。 

   - -u（unlock） 口令解锁。 

   - -d（HOME_DIR） 使账号无口令。 

   - -f 强迫用户下次登录时修改口令。 

   &emsp;&emsp;普通用户修改自己的口令时，passwd 命令会先询问原口令，验证后再要求用户输入两遍新口令，如果两次输入的口令一致，则将这个口令指定给用户；而超级用户为用户指定口令时， 就不需要知道原口令。 为了系统安全起见，用户应该选择比较复杂的口令，例如最好使用 8 位长的口令，口令中 包含有大写、小写字母和数字，并且应该与姓名、生日等不相同。 为用户指定空口令时，执行下列形式的命令： 

​		![image-20211205173939418](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211205173939418.png)

​		如果不跟用户名，则默认修改当前用户的密码。

### 2.3 Linux系统用户组的管理

&emsp;&emsp;每个用户都有一个用户组，系统可以对一个用户组中的所有用户进行集中管理。 不同 Linux 系统对用户组的规定有所不同，如 Linux 下的用户属于与它**同名的用户组**，这个用户组在创建用户时同时创建。用户组的管理涉及用户组的添加、删除和修改。组的增加、删除和修改实际上就是对`/etc/group`文件的更新。

1. 增加一个新的用户组使用groupadd命令

   其命令格式为：`groupadd 选项用户组`

   参数选项：

   - -g ：指定新用户组的组标识号（GID）
   - -o ：一般与-g 选项同时使用，表示新用户组的 GID 可以与系统已有用户组的 GID 相同

   **实例 1**：添加用户组 group1：`groupadd group1`

   ```
   [root@node1 ~]# groupadd group1
   [root@node1 ~]# cat /etc/group
   root:x:0:gem,gen
   bin:x:1:bin,daemon
   daemon:x:2:bin,daemon
   sys:x:3:bin,adm
   adm:x:4:adm,daemon,gem,gen
   tty:x:5:
   disk:x:6:
   lp:x:7:daemon
   mem:x:8:
   kmem:x:9:
   wheel:x:10:
   mail:x:12:mail,postfix
   uucp:x:14:
   man:x:15:
   games:x:20:
   gopher:x:30:
   video:x:39:
   dip:x:40:
   ftp:x:50:
   lock:x:54:
   audio:x:63:
   nobody:x:99:
   users:x:100:
   floppy:x:19:
   vcsa:x:69:
   utmp:x:22:
   utempter:x:35:
   cdrom:x:11:
   tape:x:33:
   dialout:x:18:
   saslauth:x:76:
   postdrop:x:90:
   postfix:x:89:
   fuse:x:499:
   sshd:x:74:
   apache:x:48:
   ntp:x:38:
   lucy:x:504:
   liujie:x:506:
   group1:x:507:
   ```

   此命令向系统中增加了一个新组 group1，新组的组标识号是在当前已有的最大组标识号的基础上加 1。 

   **实例 2**：向系统中增加了一个新组 group2，同时指定新组的组标识号是 101

   ```
   groupadd -g 101 group2
   ```

   ```
   [root@node1 ~]# groupadd -g 101 group2
   [root@node1 ~]# cat /etc/group
   root:x:0:gem,gen
   bin:x:1:bin,daemon
   daemon:x:2:bin,daemon
   sys:x:3:bin,adm
   adm:x:4:adm,daemon,gem,gen
   tty:x:5:
   disk:x:6:
   lp:x:7:daemon
   mem:x:8:
   kmem:x:9:
   wheel:x:10:
   mail:x:12:mail,postfix
   uucp:x:14:
   man:x:15:
   games:x:20:
   gopher:x:30:
   video:x:39:
   dip:x:40:
   ftp:x:50:
   lock:x:54:
   audio:x:63:
   nobody:x:99:
   users:x:100:
   floppy:x:19:
   vcsa:x:69:
   utmp:x:22:
   utempter:x:35:
   cdrom:x:11:
   tape:x:33:
   dialout:x:18:
   saslauth:x:76:
   postdrop:x:90:
   postfix:x:89:
   fuse:x:499:
   sshd:x:74:
   apache:x:48:
   ntp:x:38:
   lucy:x:504:
   liujie:x:506:
   group1:x:507:
   group2:x:101:
   ```

2. 如果要删除一个已有的用户组，使用 groupdel命令，其命令格式为：

   ```
   groupdel 用户组
   ```

   **实例 1**：从系统中删除组 group1

   ```
   [root@node1 ~]# groupdel group1
   [root@node1 ~]# cat /etc/group
   root:x:0:gem,gen
   bin:x:1:bin,daemon
   daemon:x:2:bin,daemon
   sys:x:3:bin,adm
   adm:x:4:adm,daemon,gem,gen
   tty:x:5:
   disk:x:6:
   lp:x:7:daemon
   mem:x:8:
   kmem:x:9:
   wheel:x:10:
   mail:x:12:mail,postfix
   uucp:x:14:
   man:x:15:
   games:x:20:
   gopher:x:30:
   video:x:39:
   dip:x:40:
   ftp:x:50:
   lock:x:54:
   audio:x:63:
   nobody:x:99:
   users:x:100:
   floppy:x:19:
   vcsa:x:69:
   utmp:x:22:
   utempter:x:35:
   cdrom:x:11:
   tape:x:33:
   dialout:x:18:
   saslauth:x:76:
   postdrop:x:90:
   postfix:x:89:
   fuse:x:499:
   sshd:x:74:
   apache:x:48:
   ntp:x:38:
   lucy:x:504:
   liujie:x:506:
   group2:x:101:
   ```

​		如果用户组下有用户是不能被删除的

​		![image-20211205193750115](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211205193750115.png)

3. 修改用户组的属性使用groupmod 命令，其命令格式为：

   ```
   groupmod 选项用户组
   ```

   **参数选项**：

   - -g：为用户组指定新的组标识号。 
   - -o ：与-g 选项同时使用，用户组的新 GID 可以与系统已有用户组的 GID 相同。 
   - -n：新用户组将用户组的名字改为新名字

   **实例 1**：将组 group2 的组标识号修改为 102

   ```
   [root@node1 ~]# groupmod -g 102 group2
   [root@node1 ~]# cat /etc/group
   root:x:0:gem,gen
   bin:x:1:bin,daemon
   daemon:x:2:bin,daemon
   sys:x:3:bin,adm
   adm:x:4:adm,daemon,gem,gen
   tty:x:5:
   disk:x:6:
   lp:x:7:daemon
   mem:x:8:
   kmem:x:9:
   wheel:x:10:
   mail:x:12:mail,postfix
   uucp:x:14:
   man:x:15:
   games:x:20:
   gopher:x:30:
   video:x:39:
   dip:x:40:
   ftp:x:50:
   lock:x:54:
   audio:x:63:
   nobody:x:99:
   users:x:100:
   floppy:x:19:
   vcsa:x:69:
   utmp:x:22:
   utempter:x:35:
   cdrom:x:11:
   tape:x:33:
   dialout:x:18:
   saslauth:x:76:
   postdrop:x:90:
   postfix:x:89:
   fuse:x:499:
   sshd:x:74:
   apache:x:48:
   ntp:x:38:
   lucy:x:504:
   liujie:x:506:
   group2:x:102:
   ```

   **实例 2**：将组 group2 的标识号改为 10000，组名修改为 group3

   ```
   [root@node1 ~]# groupmod -g 10000 -n group3 group2
   [root@node1 ~]# cat /etc/group
   root:x:0:gem,gen
   bin:x:1:bin,daemon
   daemon:x:2:bin,daemon
   sys:x:3:bin,adm
   adm:x:4:adm,daemon,gem,gen
   tty:x:5:
   disk:x:6:
   lp:x:7:daemon
   mem:x:8:
   kmem:x:9:
   wheel:x:10:
   mail:x:12:mail,postfix
   uucp:x:14:
   man:x:15:
   games:x:20:
   gopher:x:30:
   video:x:39:
   dip:x:40:
   ftp:x:50:
   lock:x:54:
   audio:x:63:
   nobody:x:99:
   users:x:100:
   floppy:x:19:
   vcsa:x:69:
   utmp:x:22:
   utempter:x:35:
   cdrom:x:11:
   tape:x:33:
   dialout:x:18:
   saslauth:x:76:
   postdrop:x:90:
   postfix:x:89:
   fuse:x:499:
   sshd:x:74:
   apache:x:48:
   ntp:x:38:
   lucy:x:504:
   liujie:x:506:
   group3:x:10000:
   ```

4. 如果一个用户同时属于多个用户组，那么用户可以在用户组之间切换，以便具有其他用户组的权限。用户可以在登录后，使用命令 newgrp 切换到其他用户组，这个命令的参数就是目的用户组。其命令格式为：

   ```
   newgrp 目的用户组
   ```

### 2.4 与用户账号、用户组有关的系统文件详讲

&emsp;&emsp;完成用户管理的工作有许多种方法，但是每一种方法实际上都是对有关的系统文件进行修改。与用户和用户组相关的信息都存放在一些系统文件中，这些文件包括 /etc/passwd, /etc/shadow, /etc/group 等。下面分别介绍这些文件的内容:

1. /etc/passwd

   /etc/passwd 文件是用户管理工作涉及的最重要的一个文件。 Linux 系统中的每个用户都在/etc/passwd 文件中有一个对应的记录行，它记录了这个**用户的一些基本属性**。 这个文件对所有用户都是**可读的**。

   ```
   [root@node1 ~]# cat /etc/passwd
   root:x:0:0:root:/root:/bin/bash
   bin:x:1:1:bin:/bin:/sbin/nologin
   daemon:x:2:2:daemon:/sbin:/sbin/nologin
   adm:x:3:4:adm:/var/adm:/sbin/nologin
   lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
   sync:x:5:0:sync:/sbin:/bin/sync
   shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
   halt:x:7:0:halt:/sbin:/sbin/halt
   mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
   uucp:x:10:14:uucp:/var/spool/uucp:/sbin/nologin
   operator:x:11:0:operator:/root:/sbin/nologin
   games:x:12:100:games:/usr/games:/sbin/nologin
   gopher:x:13:30:gopher:/var/gopher:/sbin/nologin
   ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin
   nobody:x:99:99:Nobody:/:/sbin/nologin
   vcsa:x:69:69:virtual console memory owner:/dev:/sbin/nologin
   saslauth:x:499:76:"Saslauthd user":/var/empty/saslauth:/sbin/nologin
   postfix:x:89:89::/var/spool/postfix:/sbin/nologin
   sshd:x:74:74:Privilege-separated SSH:/var/empty/sshd:/sbin/nologin
   apache:x:48:48:Apache:/var/www:/sbin/nologin
   ntp:x:38:38::/etc/ntp:/sbin/nologin
   gem:x:503:0::/home/gem:/bin/bash
   lucy:x:504:504::/usr/lucy:/bin/bash
   gen:x:505:0::/home/gen:/bin/bash
   liujie:x:506:506::/home/liujie:/bin/bash
   ```

   从上面的例子我们可以看到，/etc/passwd 中一行记录对应着一个用户，每行记录又被冒号(:)分隔为 7 个字段，

   其**格式和具体含义**如下：

   ```
   用户名:口令:用户标识号:组标识号:注释性描述:主目录:登录 Shell
   ```

   - "用户名"是代表用户账号的字符串。

     通常长度不超过 8 个字符，并且由大小写字母和/或数字组成。登录名中不能有冒号(:)，因为冒号在这里是分隔符。

   - “口令”一些系统中，存放着加密后的用户口令字。

     虽然这个字段存放的只是用户口令的加密串，不是明文，但是由于/etc/passwd文件对所有用户都可读，所以这仍是一个安全隐患。因此，现在许多 Linux 系统（如 SVR4）都使用了 shadow 技术，把真正的加密后的用户口令字存放到 /etc/shadow 文件中，而在/etc/passwd 文件的口令字段中只存放一个特殊的字 符，例如“x”或者“*”。

   - “用户标识号”是一个整数，系统内部用它来标识用户。

     一般情况下它与用户名是一一对应的。如果几个用户名对应的用户标识号是一样的，系统内部将把它们视为同一个用户，但是它们可以有不同的口令、不同的主目录以及不同的登录 Shell 等。 

     通常用户标识号的取值范围是 0～65535。0 是超级用户 root 的标识号，1～99 由系统保留，作为管理账号，普通用户的标识号从 100 开始。在 Linux 系统中，这个界限是 500。

   - “组标识号”字段记录的是用户所属的用户组。

     它对应着/etc/group 文件中的一条记录。

   - “注释性描述”字段记录着用户的一些个人情况。

     例如用户的真实姓名、电话、地址等，这个字段并没有什么实际的用途。在不同的 Linux 系统中，这个字段的格式并没有统一。在许多 Linux 系统中，这个字段存放的是一段任意的注释性描述文字，用做 finger 命令的输出。

   - “主目录”，也就是用户的起始工作目录。

     它是用户在登录到系统之后所处的目录。在大多数系统中，各用户的主目录都被组织在同一个特定的目录下，而用户主目录的名称就是该用户的登录名。各用户对自己的主目录有读、写、执行（搜索）权限，其他用户对此目录的访问权限则根据具体情况设置。

   - 用户登录后，要启动一个进程，负责将用户的操作传给内核，这个进程是用户登录到系统后运行的命令解释器或某个特定的程序，即 Shell。

     &emsp;&emsp;Shell 是用户与 Linux 系统之间的接口。Linux 的 Shell 有许多种，每种都有不同的特点。常用的有 sh(Bourne Shell), csh(C Shell), ksh(Korn Shell), tcsh(TENEX/TOPS-20 type C Shell), bash(Bourne Again Shell)等。 

     &emsp;&emsp;系统管理员可以根据系统情况和用户习惯为用户指定某个 Shell。如果不指定Shell，那么系统使用 sh(或 bash)为默认的登录 Shell，即这个字段的值为 /bin/sh。 用户的登录 Shell 也可以指定为某个特定的程序（此程序不是一个命令解释器）。 利用这一特点，我们可以限制用户只能运行指定的应用程序，在该应用程序运行结束后，用户就自动退出了系统。有些 Linux 系统要求只有那些在系统中登记了的程序才能出现在这个字段中。

   系统中有一类用户称为伪用户。这些用户在/etc/passwd 文件中也占有一条记录，但是不能登录，因为它们的登录 Shell 为空。它们的存在主要是方便系统管理，满足相应的系统进程对文件属主的要求。 

   常见的伪用户如下所示：

   ```
   		伪用户含义 
   bin     拥有可执行的用户命令文件 
   sys     拥有系统文件
   adm     拥有帐户文件 
   uucp    UUCP 使用 
   lp      lp 或 lpd 子系统使用 
   nobody  NFS 使用
   ```

   **注意**：

   &emsp;&emsp;除了上面列出的伪用户外，还有许多标准的伪用户，例如：audit, cron, mail, usenet 等，它们也都各自为相关的进程和文件所需要。 

2. /etc/shadow

   &emsp;&emsp;由于/etc/passwd 文件是所有用户都可读的，如果用户的密码太简单或规律比较 明显的话，一台普通的计算机就能够很容易地将它破解，因此对安全性要求较高的 Linux 系统都把加密后的口令字分离出来，单独存放在一个文件中，这个文件 是/etc/shadow 文件。 有超级用户才拥有该文件读权限，这就保证了用户密码 的安全性。/etc/shadow 中的记录行与/etc/passwd 中的一一对应，它由 pwconv 命令根据 /etc/passwd 中的数据自动产生 。

   ```
   [root@node1 ~]# cat /etc/shadow
   root:$6$dsDWT7lzGsD8ln//$nQoFf7GlircT8wOKmU7XmNXZ1ShgzJXnHEY1HvpagpUBxuvkfjfjMg0Gam0Je/UK./zGNfc/Ks6CWkMjxLvo7.:18952:0:99999:7:::
   bin:*:15980:0:99999:7:::
   daemon:*:15980:0:99999:7:::
   adm:*:15980:0:99999:7:::
   lp:*:15980:0:99999:7:::
   sync:*:15980:0:99999:7:::
   shutdown:*:15980:0:99999:7:::
   halt:*:15980:0:99999:7:::
   mail:*:15980:0:99999:7:::
   uucp:*:15980:0:99999:7:::
   operator:*:15980:0:99999:7:::
   games:*:15980:0:99999:7:::
   gopher:*:15980:0:99999:7:::
   ftp:*:15980:0:99999:7:::
   nobody:*:15980:0:99999:7:::
   vcsa:!!:18952::::::
   saslauth:!!:18952::::::
   postfix:!!:18952::::::
   sshd:!!:18952::::::
   apache:!!:18952::::::
   ntp:!!:18953::::::
   gem:!!:18959:0:99999:7:::
   lucy:!!:18966:0:99999:7:::
   gen:!!:18966:0:99999:7:::
   liujie:$6$cBdXCVPh$uKAACbsfctHh5GGLMKrh036NzSLr4A9PvyoHReyNf10jGl/FcXMPzzYICSKnaNy3EX/brh9olVynTwfOag.F30:18966:0:99999:7:::
   ```

   从上面例子可以看出，它的文件格式与/etc/passwd 类似，由若干个字段组成，字段之间用":"隔开。

   其**格式与具体含义**如下：

   ```
   登录名:加密口令:最后一次修改时间:最小时间间隔:最大时间间隔:警告时 间:不活动时间:失效时间:标志
   ```

   - "登录名"是与/etc/passwd 文件中的登录名相一致的用户账号
   - "口令"字段存放的是加密后的用户口令字。如果为空，则对应用户没有口令，登录时不需要口令；如果含有不属于集合 { ./0-9A-Za-z }中的字符，则对应的用户不能登录。
   - "最后一次修改时间"表示的是从某个时刻起，到用户最后一次修改口令时的天数。 时间起点对不同的系统可能不一样。例如在 SCO Linux 中，这个时间起点是 1970 年 1 月 1 日。
   - "最小时间间隔"指的是两次修改口令之间所需的最小天数。
   - "最大时间间隔"指的是口令保持有效的最大天数。
   - "警告时间"字段表示的是从系统开始警告用户到用户密码正式失效之间的天数。
   - "不活动时间"表示的是用户没有登录活动但账号仍能保持有效的最大天数
   - "失效时间"字段给出的是一个绝对的天数，如果使用了这个字段，那么就给出相应账号的生存期。期满后，该账号就不再是一个合法的账号，也就不能再用来登录了。

3. /etc/group

   &emsp;&emsp;用户组的所有信息都存放在/etc/group 文件中。将用户分组是 Linux 系统中对用户进行管理及控制访问权限的一种手段。 每个用户都属于某个用户组；一个组中可以有多个用户，一个用户也可以属于不同的组。

   &emsp;&emsp;当一个用户同时是多个组中的成员时，在/etc/passwd 文件中记录的是用户所属的主组，也就是登录时所属的默认组，而其他组称为附加组。用户要访问属于附加组的文件时，必须首先使用 `newgrp` 命令使自己成为所要访问的组中的成员。

   ```
   [root@node1 ~]# cat /etc/group
   root:x:0:gem,gen
   bin:x:1:bin,daemon
   daemon:x:2:bin,daemon
   sys:x:3:bin,adm
   adm:x:4:adm,daemon,gem,gen
   tty:x:5:
   disk:x:6:
   lp:x:7:daemon
   mem:x:8:
   kmem:x:9:
   wheel:x:10:
   mail:x:12:mail,postfix
   uucp:x:14:
   man:x:15:
   games:x:20:
   gopher:x:30:
   video:x:39:
   dip:x:40:
   ftp:x:50:
   lock:x:54:
   audio:x:63:
   nobody:x:99:
   users:x:100:
   floppy:x:19:
   vcsa:x:69:
   utmp:x:22:
   utempter:x:35:
   cdrom:x:11:
   tape:x:33:
   dialout:x:18:
   saslauth:x:76:
   postdrop:x:90:
   postfix:x:89:
   fuse:x:499:
   sshd:x:74:
   apache:x:48:
   ntp:x:38:
   lucy:x:504:
   liujie:x:506:
   group3:x:10000:
   ```

   用户组的所有信息都存放在/etc/group 文件中。此文件的格式也类似于 /etc/passwd 文件，由冒号(:)隔开若干个字段。

   其**格式与具体含义**如下：

   ```
   组名:口令:组标识号:组内用户列表
   ```

   - "组名"是用户组的名称，由字母或数字构成。与/etc/passwd 中的登录名一样，组名不应重复。
   - "口令"字段存放的是用户组加密后的口令字。一般 Linux 系统的用户组都没有口令，即这个字段一般为空，或者是*。
   - "组标识号"与用户标识号类似，也是一个整数，被系统内部用来标识组。
   - "组内用户列表"是属于这个组的所有用户的列表，不同用户之间用逗号(,)分隔。这个用户组可能是用户的主组，也可能是附加组。 

## 3.Linux文件属性及权限

### 3.1 Linux文件属性与权限

&emsp;&emsp;文件属性主要包括：文件所属的用户与文件所属的用户组、文件的修改时间、文件的大小等。我们可以以 root 用户的身份登陆 Linux,执行命令：`ls -al` 来查看文件

​		![image-20211205210222296](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211205210222296.png)

`ls` 是 list 的缩写,能显示文件的文件名和相关属性。而`-al` 表示列出所有的文件详细的权限与属性(包含隐藏文件,诸如文件名以“.”开头的文件)。

**显示信息的具体含义**如下：

![image-20211205210501340](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211205210501340.png)

1. 第一列：**文件的类型与权限**共有 10 个字符。

![image-20211205210719129](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211205210719129.png)

- 第一个字符代表这个文件的具体类型
  - 若是[d]，则是目录（directory） 
  - 若是[-]，则是文件 
  - 若是[l]，则是链接文件（link） 。类似 window 系统下的快捷方式。
  - 设备与设备文件[b]、[c]：与系统外设和存储等相关的一些文件，通常都集中在/dev 这个目录中。分为两种：
    - 块（block）设备文件[b]：就是一些存储数据，以提供系统随机访问的接口设备，例如硬盘、软盘等。你可以随机的在硬盘的不同块读写，这种设备就是成组设备。
    - 字符（character）设备文件[c]：是一些串行端口的接口设备,例如键盘,鼠标。这些设备的特征就是“一次性读取”的，不能够截断输出。

- 余下的字符,三个一组,且均为[rwx]的 3 个参数组合；其中[r]代表可读(read),[w]代表可写(write),[x]代表可执行(excute)

  &emsp;&emsp;这三个参数的出现顺序不会改变,若没有某个权限,则会以[-]代替。这三组参数中,第一组是文件所有者的权限;第二组是同用户组的权限;第三组是其他用户的权限。这三组权限均是针对某些账号而言的权限。另外,文件权限和目录权限意义不同,这是因为文件与目录记录的数据内容不相同。

2. 第二列表示有多少文件名链接到此节点(i-node)。

   每个文件都会将他的权限与属性记录到文件系统的i-node 中,但是 Linux所使用的目录树却是使用文件名来记录,因此每个文件名就会链接到一个 i-node。这个属性记录的就是有多少不同的文件名链接到相同的一个 i-node 号码。

3. 第三列表示这个文件(或目录)的所有者账号

4. 第四列表示这个文件的所属用户组，在 Linux 系统下,每个账号会附属于一个或多个用户组中

5. 第五列表述这个文件的容量大小,默认单位为 byte

6. 第六列为这个文件的创建文件日期或者是最近的修改日期

   如果想要显示完整的时间格式,可以使用ls参数,即`ls -l --full-time`,这样做就可以显示出完整的时间格式。

7. 第七列为该文件名 

   前缀为”.”的是隐藏文件

&emsp;&emsp;Linux 文件权限最大的用途在于数据安全性上,它能根据不同用户的不同权限实现对不同文件的操作。为此,在我们设置 Linux 文件与目录的属性之前,需要弄清到底什么数据是可变的,什么数据是不可变的。

### 3.2 如何改变文件属性与权限

&emsp;&emsp;下面介绍几个常用于用户组,所有者,各种身份的权限的修改的命令:

- chgrp:改变文件所属用户组 
- chown:改变文件所有者 
- chmod:改变文件的权限 

#### 3.2.1 改变所属用户组

&emsp;&emsp;改变所属用户组的命令格式：`chgrp [-R] 用户组 dirname/filename`。如果为目录，则需要`-R`递归修改组。

**注意：**需要注意的是,要被改的组名必须要在/etc/group 文件内存在才行,否则会报错

**实例1**：将根目录下的ok1.log文件的用户组由root改为lucy

```
[root@node1 ~]# ll
总用量 14108
-rw-------. 1 root root     900 11月 22 03:24 anaconda-ks.cfg
drwxr-xr-x  8 root root    4096 11月 25 18:26 dirtest
-rw-r--r--  1 root root     139 11月 27 16:41 emp.txt
-rw-r--r--  1 root root 8527322 11月 27 21:14 etc01.tar.gz
-rw-r--r--  1 root root 5849088 11月 27 21:15 etc02.tar.gz
-rw-r--r--  1 root root     144 11月 27 14:28 hello.txt
-rw-r--r--  1 root root     884 11月 26 20:47 inittab
-rw-r--r--. 1 root root    8815 11月 22 03:24 install.log
-rw-r--r--. 1 root root    3384 11月 22 03:24 install.log.syslog
-rw-r--r--. 1 root root    1796 10月  2 2013 ln1
lrwxrwxrwx  1 root root       7 11月 25 18:30 ln2 -> profile
-rw-r--r--  1 root root       0 11月 25 21:36 new.log
-rw-r--r--  1 root root    1659 11月 26 16:35 ok1.log
-rw-r--r--  1 root root    1105 11月 26 16:42 ok2.log
-rw-r--r--  1 root root    1015 11月 27 14:43 passwd
-rw-r--r--  1 root root    1796 11月 25 21:37 profile
-rw-r--r--  1 root root     144 11月 27 14:26 profile_path.txt
-rw-r--r--  1 root root     191 11月 26 20:32 sed.txt
-rw-r--r--  1 root root      66 11月 26 18:50 sort.txt
[root@node1 ~]# chgrp lucy ok1.log 
[root@node1 ~]# ll
总用量 14108
-rw-------. 1 root root     900 11月 22 03:24 anaconda-ks.cfg
drwxr-xr-x  8 root root    4096 11月 25 18:26 dirtest
-rw-r--r--  1 root root     139 11月 27 16:41 emp.txt
-rw-r--r--  1 root root 8527322 11月 27 21:14 etc01.tar.gz
-rw-r--r--  1 root root 5849088 11月 27 21:15 etc02.tar.gz
-rw-r--r--  1 root root     144 11月 27 14:28 hello.txt
-rw-r--r--  1 root root     884 11月 26 20:47 inittab
-rw-r--r--. 1 root root    8815 11月 22 03:24 install.log
-rw-r--r--. 1 root root    3384 11月 22 03:24 install.log.syslog
-rw-r--r--. 1 root root    1796 10月  2 2013 ln1
lrwxrwxrwx  1 root root       7 11月 25 18:30 ln2 -> profile
-rw-r--r--  1 root root       0 11月 25 21:36 new.log
-rw-r--r--  1 root lucy    1659 11月 26 16:35 ok1.log
-rw-r--r--  1 root root    1105 11月 26 16:42 ok2.log
-rw-r--r--  1 root root    1015 11月 27 14:43 passwd
-rw-r--r--  1 root root    1796 11月 25 21:37 profile
-rw-r--r--  1 root root     144 11月 27 14:26 profile_path.txt
-rw-r--r--  1 root root     191 11月 26 20:32 sed.txt
-rw-r--r--  1 root root      66 11月 26 18:50 sort.txt
```

#### 3.2.2 改变文件所有者

&emsp;&emsp;使用 chown 命令可以改变一个文件的所有者,还可以直接修改群组的名称，其命令格式为：

```
chown [-R] 用户账号 dirname/filename
or
chown [-R] 用户账号：用户组名 dirname/filename
```

**注意：**用户必须是已经存在于系统中的账号,也就是在/etc/passwd 这个文件中有记录的用户名称才能改变。如果要将目录下的所有子文件或目录同时改变文件所有者,加`-R` 参数即可。

**实例1**：将 ok1.log 的所有者改为 bin 用户

```
[root@node1 ~]# ll
总用量 14108
-rw-------. 1 root root     900 11月 22 03:24 anaconda-ks.cfg
drwxr-xr-x  8 root root    4096 11月 25 18:26 dirtest
-rw-r--r--  1 root root     139 11月 27 16:41 emp.txt
-rw-r--r--  1 root root 8527322 11月 27 21:14 etc01.tar.gz
-rw-r--r--  1 root root 5849088 11月 27 21:15 etc02.tar.gz
-rw-r--r--  1 root root     144 11月 27 14:28 hello.txt
-rw-r--r--  1 root root     884 11月 26 20:47 inittab
-rw-r--r--. 1 root root    8815 11月 22 03:24 install.log
-rw-r--r--. 1 root root    3384 11月 22 03:24 install.log.syslog
-rw-r--r--. 1 root root    1796 10月  2 2013 ln1
lrwxrwxrwx  1 root root       7 11月 25 18:30 ln2 -> profile
-rw-r--r--  1 root root       0 11月 25 21:36 new.log
-rw-r--r--  1 root lucy    1659 11月 26 16:35 ok1.log
-rw-r--r--  1 root root    1105 11月 26 16:42 ok2.log
-rw-r--r--  1 root root    1015 11月 27 14:43 passwd
-rw-r--r--  1 root root    1796 11月 25 21:37 profile
-rw-r--r--  1 root root     144 11月 27 14:26 profile_path.txt
-rw-r--r--  1 root root     191 11月 26 20:32 sed.txt
-rw-r--r--  1 root root      66 11月 26 18:50 sort.txt
[root@node1 ~]# chown bin ok1.log 
[root@node1 ~]# ll
总用量 14108
-rw-------. 1 root root     900 11月 22 03:24 anaconda-ks.cfg
drwxr-xr-x  8 root root    4096 11月 25 18:26 dirtest
-rw-r--r--  1 root root     139 11月 27 16:41 emp.txt
-rw-r--r--  1 root root 8527322 11月 27 21:14 etc01.tar.gz
-rw-r--r--  1 root root 5849088 11月 27 21:15 etc02.tar.gz
-rw-r--r--  1 root root     144 11月 27 14:28 hello.txt
-rw-r--r--  1 root root     884 11月 26 20:47 inittab
-rw-r--r--. 1 root root    8815 11月 22 03:24 install.log
-rw-r--r--. 1 root root    3384 11月 22 03:24 install.log.syslog
-rw-r--r--. 1 root root    1796 10月  2 2013 ln1
lrwxrwxrwx  1 root root       7 11月 25 18:30 ln2 -> profile
-rw-r--r--  1 root root       0 11月 25 21:36 new.log
-rw-r--r--  1 bin  lucy    1659 11月 26 16:35 ok1.log
-rw-r--r--  1 root root    1105 11月 26 16:42 ok2.log
-rw-r--r--  1 root root    1015 11月 27 14:43 passwd
-rw-r--r--  1 root root    1796 11月 25 21:37 profile
-rw-r--r--  1 root root     144 11月 27 14:26 profile_path.txt
-rw-r--r--  1 root root     191 11月 26 20:32 sed.txt
-rw-r--r--  1 root root      66 11月 26 18:50 sort.txt
```

#### 3.2.3 改变权限

&emsp;&emsp;使用的是 chmod命令可以改变文件或目录权限。其命令格式为：

```
chmod [-R] mode dirname/filename
```

但是权限的设置方法分两种,可以通过数字或符号进行修改。下面分别介绍这两种方法：

1. 数字类型改变文件权限

   Linux 的 基 本 权 限 有 9 个 , 分 别 是 owner,group,others 三 种 身 份 各 自 的 

   read,write,excute 权限,各个权限对应的数字如下:

   ```
   r:4、w:2、x:1
   rw- 6 
   r-x 5 
   r-- 4 
   --x 1
   ```

   为此每种身份各自的三个权限数字相加即可得出数字表示的权限，例如：`[-rw-r-----]`
   $$
   owner = rwx = 4+2+1 = 7\\group = r-x = 4+1 = 5\\others=---= 0+0+0 = 0
   $$
   实例1：修改文件权限,改为 750 即可

   ```\
   [root@node1 ~]# ll
   总用量 14108
   -rw-------. 1 root root     900 11月 22 03:24 anaconda-ks.cfg
   drwxr-xr-x  8 root root    4096 11月 25 18:26 dirtest
   -rw-r--r--  1 root root     139 11月 27 16:41 emp.txt
   -rw-r--r--  1 root root 8527322 11月 27 21:14 etc01.tar.gz
   -rw-r--r--  1 root root 5849088 11月 27 21:15 etc02.tar.gz
   -rw-r--r--  1 root root     144 11月 27 14:28 hello.txt
   -rw-r--r--  1 root root     884 11月 26 20:47 inittab
   -rw-r--r--. 1 root root    8815 11月 22 03:24 install.log
   -rw-r--r--. 1 root root    3384 11月 22 03:24 install.log.syslog
   -rw-r--r--. 1 root root    1796 10月  2 2013 ln1
   lrwxrwxrwx  1 root root       7 11月 25 18:30 ln2 -> profile
   -rw-r--r--  1 root root       0 11月 25 21:36 new.log
   -rw-r--r--  1 bin  lucy    1659 11月 26 16:35 ok1.log
   -rw-r--r--  1 root root    1105 11月 26 16:42 ok2.log
   -rw-r--r--  1 root root    1015 11月 27 14:43 passwd
   -rw-r--r--  1 root root    1796 11月 25 21:37 profile
   -rw-r--r--  1 root root     144 11月 27 14:26 profile_path.txt
   -rw-r--r--  1 root root     191 11月 26 20:32 sed.txt
   -rw-r--r--  1 root root      66 11月 26 18:50 sort.txt
   [root@node1 ~]# chmod 750 /root/ok1.log 
   [root@node1 ~]# ll
   总用量 14108
   -rw-------. 1 root root     900 11月 22 03:24 anaconda-ks.cfg
   drwxr-xr-x  8 root root    4096 11月 25 18:26 dirtest
   -rw-r--r--  1 root root     139 11月 27 16:41 emp.txt
   -rw-r--r--  1 root root 8527322 11月 27 21:14 etc01.tar.gz
   -rw-r--r--  1 root root 5849088 11月 27 21:15 etc02.tar.gz
   -rw-r--r--  1 root root     144 11月 27 14:28 hello.txt
   -rw-r--r--  1 root root     884 11月 26 20:47 inittab
   -rw-r--r--. 1 root root    8815 11月 22 03:24 install.log
   -rw-r--r--. 1 root root    3384 11月 22 03:24 install.log.syslog
   -rw-r--r--. 1 root root    1796 10月  2 2013 ln1
   lrwxrwxrwx  1 root root       7 11月 25 18:30 ln2 -> profile
   -rw-r--r--  1 root root       0 11月 25 21:36 new.log
   -rwxr-x---  1 bin  lucy    1659 11月 26 16:35 ok1.log
   -rw-r--r--  1 root root    1105 11月 26 16:42 ok2.log
   -rw-r--r--  1 root root    1015 11月 27 14:43 passwd
   -rw-r--r--  1 root root    1796 11月 25 21:37 profile
   -rw-r--r--  1 root root     144 11月 27 14:26 profile_path.txt
   -rw-r--r--  1 root root     191 11月 26 20:32 sed.txt
   -rw-r--r--  1 root root      66 11月 26 18:50 sort.txt
   ```

2. 符号类型改变文件权限

   Linux总共 9种权限,对应着三种身份, 为此我们可以通过 u,g,o 代表三种身份,另外 a 代表全部身份。对应的权限可以写为 r,w,x。如下图所示：

   ![image-20211205220356481](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112062214097.png)

   **实例1**：将一个文件的权限改做[-rwxr-xr-x],具体来说就是: user(u):具有可读可写可执行权限; group 与 others:具有可读可执行权限

   ```
   [root@node1 ~]# ls -al /root/.bashrc 
   -rw-r--r--. 1 root root 176 9月  23 2004 /root/.bashrc
   [root@node1 ~]# chmod u=rwx,go=rx .bashrc
   [root@node1 ~]# ls -al /root/.bashrc 
   -rwxr-xr-x. 1 root root 176 9月  23 2004 /root/.bashrc
   ```

   要去掉权限但不修改其他权限,例如去掉全部的可执行权限:

   ```
   [root@node1 ~]# ls -al /root/.bashrc 
   -rwxr-xr-x. 1 root root 176 9月  23 2004 /root/.bashrc
   [root@node1 ~]# chmod a-x .bashrc 
   [root@node1 ~]# ls -al /root/.bashrc 
   -rw-r--r--. 1 root root 176 9月  23 2004 /root/.bashrc
   ```

   添加全部可执行权限

   ```
   [root@node1 ~]# ls -al /root/.bashrc 
   -rw-r--r--. 1 root root 176 9月  23 2004 /root/.bashrc
   [root@node1 ~]# chmod a+x .bashrc 
   [root@node1 ~]# ls -al /root/.bashrc 
   -rwxr-xr-x. 1 root root 176 9月  23 2004 /root/.bashrc
   ```

   恢复原权限

   ```
   [root@node1 ~]# chmod 664 .bashrc 
   [root@node1 ~]# ls -al /root/.bashrc 
   -rw-rw-r--. 1 root root 176 9月  23 2004 /root/.bashrc
   ```

## 4.目录与文件权限的意义

### 4.1 权限对文件的重要性

&emsp;&emsp;文件是实际含有数据的地方,包括一般文本文件,数据库内容文件,二进制可执行文件 (binary program)等等,因此,文件权限有如下意义:

- r（read）：可读取此文件的实际内容，如读取文本文件的文字内容等； 
- w（write）：可以编辑、新增或者是修改该文件的内容 
- x（execute）：该文件具有可以被系统执行的权限。

**注意:**在 Linux 中,文件是否能被执行是由是否具有”x”这个权限来决定,与拓展名无关。

### 4.2 权限对目录的重要性

- r（read contents in directory）：表示具有读取目录结构清单的权限，所以当你具有读取（r）一个目录的权限时，表示你可以查询该目录下的文件名数据。所以你就可以利用 ls 这个指令将该目录的内容列表显示出来。
- w（modify contents of directory）：这个可写入的权限对目录来说，表示你具有改变目录结构清单的权限，也就是底下这些权限：
  - 建立新的文件或子目录； /opt/ 
  - 删除已经存在的文件或子目录（不论该文件的权限） 
  - 将已存在的文件或目录进行重命名； 
  - 移动该目录内的文件、目录位置

- x（access directory）：在 Linux 中,目录不可以被执行,目录的 x 代表的是使用者能否进入该目录成为工作目录的用途。所谓的工作目录（work directory）就是你目前所在的目录。举例来说，当你登入 Linux 时，你所在的 Home 目录就是你当下的工作目录。

![image-20211206131451200](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112061314343.png)

## 5.Linux软件安装

&emsp;&emsp;软件安装有三种方式：

- 源码编译安装 
- rpm 安装 
- yum 安装 

前面两种是离线安装，最后一种是在线安装。

### 5.1 源码编译安装

&emsp;&emsp;以nginx的源码编译安装为例，

- 安装GCC、

  由于 Nginx 具有很强的可扩展性，需要使用哪些功能，只需通过 with 配置后重新编译即可添加此功能，所以对于 Nginx 的源码安装方式是必须要掌握的。由于 Nginx 是由C/C++语言编写的，所以对其进行编译就必须要使用相关编译器。对于 C/C++语言的编译器，使用最多的是 gcc 与 gcc-c++，而这两款编译器在 CentOS6.5 中是没有安装的，所以首先要安装这两款编译器。

- 安装依赖库

  基本的 Nginx 功能依赖于一些基本的库，在安装 Nginx 之前需要提前安装这些库。

  - .pcre-devel：pcre(Perl Compatible Regular Expressions)，Perl 脚本语言兼容正则表达式，为 Nginx 提供正则表达式库
  - openssl-devel：为 Nginx 提供 SSL（安全套接字层）密码库，包含主要的密码算法、常用的密钥和证书封装管理功能及 SSL 协议，并提供丰富的应用程序供测试或其它目的使用。在安装之前需要注意，很多库具有 devel 库与非 devel 库两种。devel 库表示 development 开发库，比非 devel 库会多出一些头文件、静态库、源码包等。而这些包在运行时不可能用到，但在开发时有可能用到。所以对于程序员来说，一般都是需要安装 devel 库的。不过，在 yum 安装 devel 库时，由于其依赖于非 devel 库，所以其会先自动安装非 devel 库，而后再安装 devel 库。所以真正在安装时，只需显示的安装 devel 库即可。通过以下命令可以查看到，非 devel 库也被安装了。

**安装步骤**如下：

1. 编译 nginx 源码

   ```
   [root@node1 ~]# yum install gcc gcc-c++
   ```

2.  用于支持 https 协议

   ```
   [root@node1 ~]# yum install openssl openssl-devel -y
   ```

3. 用于解析正则表达式

   ```
   [root@node1 ~]# yum install pcre pcre-devel -y
   ```

4. 用于压缩 gzip deflate，这个包一般系统里带了

   ```
   [root@node1 ~]# yum install zlib zlib-devel -y
   ```

5. mkdir /opt/apps,将下载好的nginx-1.8.1上传到虚拟机的/opt/apps 目录下

   下载地址为：http://nginx.org/en/download.html

​		![image-20211206135853074](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112061358025.png)

```
[root@node1 ~]# mkdir /opt/apps
[root@node1 ~]# ls /opt
apps
```

通过xftp5将下载好的内容上传到虚拟机的/opt/apps 目录下

![image-20211206140233336](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112061402570.png)

![image-20211206140610460](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112061406914.png)

6. 解压

   ```
   [root@node1 ~]# ls /opt/apps
   nginx-1.8.1.tar.gz
   [root@node1 ~]# cd /opt/apps
   [root@node1 apps]# tar -zxvf nginx-1.8.1.tar.gz
   ```

7. 配置

   ```
   [root@node1 apps]# cd nginx-1.8.1
   [root@node1 nginx-1.8.1]# ./configure --prefix=/opt/nginx1.8 --with-http_ssl_module --with-http_gzip_static_module --error-log-path=/var/log/nginx1.8/nginx.log --pid-path=/var/log/nginx1.8/pid
   ```

​	   后面会学习对nginx的配置，可以通过`./configure --help` 查看帮助信息

8. 编译

   生成脚本及配置文件：make 

   编译步骤，根据 Makefile 文件生成相应的模块

   ```
   [root@node1 nginx-1.8.1]# ll
   总用量 668
   drwxr-xr-x 6 1001 1001   4096 12月  6 14:08 auto
   -rw-r--r-- 1 1001 1001 251319 1月  26 2016 CHANGES
   -rw-r--r-- 1 1001 1001 383019 1月  26 2016 CHANGES.ru
   drwxr-xr-x 2 1001 1001   4096 12月  6 14:08 conf
   -rwxr-xr-x 1 1001 1001   2478 1月  26 2016 configure
   drwxr-xr-x 4 1001 1001   4096 12月  6 14:08 contrib
   drwxr-xr-x 2 1001 1001   4096 12月  6 14:08 html
   -rw-r--r-- 1 1001 1001   1397 1月  26 2016 LICENSE
   -rw-r--r-- 1 root root    333 12月  6 14:11 Makefile
   drwxr-xr-x 2 1001 1001   4096 12月  6 14:08 man
   drwxr-xr-x 3 root root   4096 12月  6 14:11 objs
   -rw-r--r-- 1 1001 1001     49 1月  26 2016 README
   drwxr-xr-x 8 1001 1001   4096 12月  6 14:08 src
   [root@node1 nginx-1.8.1]# make
   ```

9. 安装

   ```
   [root@node1 nginx-1.8.1]# make install
   make -f objs/Makefile install
   make[1]: Entering directory `/opt/apps/nginx-1.8.1'
   test -d '/opt/nginx1.8' || mkdir -p '/opt/nginx1.8'
   test -d '/opt/nginx1.8/sbin' 		|| mkdir -p '/opt/nginx1.8/sbin'
   test ! -f '/opt/nginx1.8/sbin/nginx' 		|| mv '/opt/nginx1.8/sbin/nginx' 		   '/opt/nginx1.8/sbin/nginx.old'
   cp objs/nginx '/opt/nginx1.8/sbin/nginx'
   test -d '/opt/nginx1.8/conf' 		|| mkdir -p '/opt/nginx1.8/conf'
   cp conf/koi-win '/opt/nginx1.8/conf'
   cp conf/koi-utf '/opt/nginx1.8/conf'
   cp conf/win-utf '/opt/nginx1.8/conf'
   test -f '/opt/nginx1.8/conf/mime.types' 		|| cp conf/mime.types '/opt/nginx1.8/conf'
   cp conf/mime.types '/opt/nginx1.8/conf/mime.types.default'
   test -f '/opt/nginx1.8/conf/fastcgi_params' 		|| cp conf/fastcgi_params '/opt/nginx1.8/conf'
   cp conf/fastcgi_params 		'/opt/nginx1.8/conf/fastcgi_params.default'
   test -f '/opt/nginx1.8/conf/fastcgi.conf' 		|| cp conf/fastcgi.conf '/opt/nginx1.8/conf'
   cp conf/fastcgi.conf '/opt/nginx1.8/conf/fastcgi.conf.default'
   test -f '/opt/nginx1.8/conf/uwsgi_params' 		|| cp conf/uwsgi_params '/opt/nginx1.8/conf'
   cp conf/uwsgi_params 		'/opt/nginx1.8/conf/uwsgi_params.default'
   test -f '/opt/nginx1.8/conf/scgi_params' 		|| cp conf/scgi_params '/opt/nginx1.8/conf'
   cp conf/scgi_params 		'/opt/nginx1.8/conf/scgi_params.default'
   test -f '/opt/nginx1.8/conf/nginx.conf' 		|| cp conf/nginx.conf '/opt/nginx1.8/conf/nginx.conf'
   cp conf/nginx.conf '/opt/nginx1.8/conf/nginx.conf.default'
   test -d '/var/log/nginx1.8' 		|| mkdir -p '/var/log/nginx1.8'
   test -d '/opt/nginx1.8/logs' || 		mkdir -p '/opt/nginx1.8/logs'
   test -d '/opt/nginx1.8/html' 		|| cp -R html '/opt/nginx1.8'
   test -d '/var/log/nginx1.8' || 		mkdir -p '/var/log/nginx1.8'
   make[1]: Leaving directory `/opt/apps/nginx-1.8.1'
   ```

   ```
   [root@node1 nginx-1.8.1]# ls /opt
   apps  nginx1.8
   [root@node1 nginx-1.8.1]# cd /opt/nginx1.8/
   [root@node1 nginx1.8]# ll
   总用量 16
   drwxr-xr-x 2 root root 4096 12月  6 14:20 conf
   drwxr-xr-x 2 root root 4096 12月  6 14:20 html
   drwxr-xr-x 2 root root 4096 12月  6 14:20 logs
   drwxr-xr-x 2 root root 4096 12月  6 14:20 sbin
   ```

   - **conf**：保存 nginx 所有的配置文件，其中 nginx.conf 是 nginx 服务器的最核心最主要的配置文件，其他的.conf 则是用来配置 nginx 相关的功能的，例如 fastcgi 功能使用的是 fastcgi.conf 和 fastcgi_params 两个文件，配置文件一般都有个样板配置文件， 是文件名.default 结尾，使用的使用将其复制为并将 default 去掉即可。
   - **html**：目录中保存了 nginx 服务器的 web 文件，但是可以更改为其他目录保存 web 文件, 另外还有一个 50x 的 web 文件是默认的错误页面提示页面。
   - **logs**：用来保存 nginx 服务器的访问日志错误日志等日志，logs 目录可以放在其他路径，比如/var/logs/nginx 里面。
   - **sbin**：保存 nginx 二进制启动脚本，可以接受不同的参数以实现不同的功能。

10. 测试

    ```
    [root@node1 nginx1.8]# cd sbin
    [root@node1 sbin]# ls
    nginx
    [root@node1 sbin]# ./nginx
    [root@node1 sbin]# ifconfig
    eth0      Link encap:Ethernet  HWaddr 00:0C:29:18:6D:FC  
              inet addr:192.168.236.101  Bcast:192.168.236.255  Mask:255.255.255.0
              inet6 addr: fe80::20c:29ff:fe18:6dfc/64 Scope:Link
              UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
              RX packets:42013 errors:0 dropped:0 overruns:0 frame:0
              TX packets:19282 errors:0 dropped:0 overruns:0 carrier:0
              collisions:0 txqueuelen:1000 
              RX bytes:58455012 (55.7 MiB)  TX bytes:1377097 (1.3 MiB)
    
    lo        Link encap:Local Loopback  
              inet addr:127.0.0.1  Mask:255.0.0.0
              inet6 addr: ::1/128 Scope:Host
              UP LOOPBACK RUNNING  MTU:16436  Metric:1
              RX packets:0 errors:0 dropped:0 overruns:0 frame:0
              TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
              collisions:0 txqueuelen:0 
              RX bytes:0 (0.0 b)  TX bytes:0 (0.0 b)
    ```

    在浏览器地址栏中输入 http://192.168.236.101 

    ![image-20211206143230010](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112061432177.png)

​	 出现该界面说明 nginx 安装成功。

### 5.2 rpm安装

&emsp;&emsp;RPM 的**优点** :

- RPM 内含已经编译过的程序与配置文件等数据,可以让用户免除重新编译的困扰 

- RPM 在被安装之前,会先检查系统的硬盘容量、操作系统版本等,可避免文件被错误安装 

- RPM 文件本身提供软件版本信息、相依属性软件名称、软件用途说明、软件所含文件等信息,便于了解软件 

- RPM 管理的方式:使用数据库记录 RPM 文件的相关参数,便于升级、移除、查询与验证

&emsp;&emsp;rpm **默认安装的路径** :

-  /etc 

  一些配置文件放置的目录,例如/etc/crontab 

-  /usr/bin 

  一些可执行文件

- /usr/lib 

  一些程序使用的动态链接库 

- /usr/share/doc 

  一些基本的软件使用手册与说明文件 

- /usr/share/man 

  一些 man page（Linux 命令的随机帮助说明）文件

手动下载RPM的网址：http://www.rpmfind.net/linux/rpm2html/search.php

![image-20211206151241548](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112061512034.png)

&emsp;&emsp;rpm安装，其命令格式为：`rpm -ivh package_name`

参数说明：

- -i :install 的意思 

- -v :察看更细部的安装信息画面 

- -h :以安装信息列显示安装进度

  ```
  # 安装单个 rpm 包 
  rpm -ivh package_name 
  # 安装多个 rpm 包 
  rpm -ivh a.i386.rpm b.i386.rpm *.rpm 
  # 安装网上某个位置 rpm 包 ,在线安装
  rpm -ivh http://website.name/path/pkgname.rpm
  ```

#### 5.2.1 rpm安装jdk

1. 将jdk上传到/opt/apps 目录下![image-20211206152021569](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112061520660.png)

   ```
   [root@node1 ~]# ls /opt/apps/
   jdk-7u80-linux-x64.rpm  nginx-1.8.1  nginx-1.8.1.tar.gz
   ```

2. 安装当前目录下的 jdk-7u80-linux-x64.rpm

   ```
   [root@node1 ~]# cd /opt/apps
   [root@node1 apps]# rpm -ivh jdk-7u80-linux-x64.rpm 
   Preparing...                ########################################### [100%]
      1:jdk                    ########################################### [100%]
   Unpacking JAR files...
   	rt.jar...
   	jsse.jar...
   	charsets.jar...
   	tools.jar...
   	localedata.jar...
   	jfxrt.jar...
   ```

3. 查找 java 安装目录的位置

   ```
   [root@node1 apps]#  whereis java
   java: /usr/bin/java
   [root@node1 apps]# ll /usr/bin/java
   lrwxrwxrwx 1 root root 26 12月  6 15:23 /usr/bin/java -> /usr/java/default/bin/java
   [root@node1 apps]# cd /usr/java
   [root@node1 java]# ls
   default  jdk1.7.0_80  latest
   [root@node1 java]# cd jdk1.7.0_80/
   [root@node1 jdk1.7.0_80]# pwd
   /usr/java/jdk1.7.0_80
   ```

4. 配置环境变量

   ```
   [root@node1 jdk1.7.0_80]# vim /etc/profile
   ```

   在文件中加入两行代码：

   ```
   export JAVA_HOME=/usr/java/jdk1.7.0_80
   export PATH=$PATH:$JAVA_HOME/bin
   ```

   ![image-20211206153602916](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112061536123.png)

5. 让配置生效，使用.命令，或者 source 命令 

   ```
   [root@node1 jdk1.7.0_80]# source /etc/profile
   ```

6. 测试安装配置是否成功

   ![image-20211206154103140](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112061541626.png)

​		安装成功！

#### 5.2.2 rpm常用组合选项

&emsp;&emsp;**rpm** **查询：** rpm 在查询的时候,其实查询的地方是在/var/lib/rpm/ 这个目录下的数据库文件 。

```
[root@node1 jdk1.7.0_80]# ll /var/lib/rpm/
总用量 28864
-rw-r--r--. 1 root root  1523712 12月  6 15:23 Basenames
-rw-r--r--. 1 root root    12288 12月  6 13:43 Conflictname
-rw-r--r--  1 root root    24576 12月  6 15:23 __db.001
-rw-r--r--  1 root root   229376 12月  6 15:23 __db.002
-rw-r--r--  1 root root  1318912 12月  6 15:23 __db.003
-rw-r--r--  1 root root   753664 12月  6 15:23 __db.004
-rw-r--r--. 1 root root   335872 12月  6 15:23 Dirnames
-rw-r--r--. 1 root root  2592768 12月  6 15:23 Filedigests
-rw-r--r--. 1 root root    12288 12月  6 15:23 Group
-rw-r--r--. 1 root root    12288 12月  6 15:23 Installtid
-rw-r--r--. 1 root root    12288 12月  6 15:23 Name
-rw-r--r--. 1 root root    12288 12月  6 13:43 Obsoletename
-rw-r--r--. 1 root root 22093824 12月  6 15:23 Packages
-rw-r--r--. 1 root root  1294336 12月  6 15:23 Providename
-rw-r--r--. 1 root root   557056 12月  6 15:23 Provideversion
-rw-r--r--. 1 root root    12288 11月 22 07:55 Pubkeys
-rw-r--r--. 1 root root   110592 12月  6 15:23 Requirename
-rw-r--r--. 1 root root    69632 12月  6 15:23 Requireversion
-rw-r--r--. 1 root root    24576 12月  6 15:23 Sha1header
-rw-r--r--. 1 root root    12288 12月  6 15:23 Sigmd5
-rw-r--r--. 1 root root    12288 12月  6 13:35 Triggername
```

**rpm 查询已安装软件,选项与参数：**

- -q :仅查询,后面接的软件名称是否有安装 
- -qa :列出所有的,已经安装在本机 Linux 系统上面的所有软件名称  
- -qi :列出该软件的详细信息,包含开发商、版本和说明等 
- -ql :列出该软件所有的文件与目录所在完整文件名 
- -qc :列出该软件的所有配置文件 
- -qd :列出该软件的所有说明文件 
- -qR :列出和该软件有关的相依软件所含的文件 
- -qf :由后面接的文件名,找出该文件属于哪一个已安装的软件 
- -qp[icdlR]: 查询某个 RPM 文件内含有的信息: 

**注意** :-qp 后面接的所有参数以上面的说明一致。但用途仅在于找出 某个 RPM 文件内的信息,而非已安装的软件信息 

#### 5.2.3 rpm安装MYSQL

&emsp;&emsp;从 MySQL 官网下载 MySQL 的安装包： https://downloads.mysql.com/archives/community/ 

需要下载 mysql 的四个 rpm 包： 

- mysql-community-client-5.7.19-1.el6.x86_64.rpm 

- mysql-community-common-5.7.19-1.el6.x86_64.rpm 

- mysql-community-libs-5.7.19-1.el6.x86_64.rpm 

- mysql-community-server-5.7.19-1.el6.x86_64.rpm
- numactl-2.0.9-2.el6.x86_64.rpm（mysql的依赖包）

![image-20211206161237962](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112061623021.png)

**安装MYSQL步骤**：

1. 查找当前系统中安装的 MySQL

   ```
   [root@node1 jdk1.7.0_80]# rpm -qa | grep mysql
   mysql-libs-5.1.71-1.el6.x86_64
   ```

2. 将上一步找到的 mysql 相关的包都删除

   ```
   [root@node1 jdk1.7.0_80]# rpm -e mysql-libs-5.1.71-1.el6.x86_64
   error: Failed dependencies:
   	libmysqlclient.so.16()(64bit) is needed by (installed) postfix-2:2.6.6-2.2.el6_1.x86_64
   	libmysqlclient.so.16(libmysqlclient_16)(64bit) is needed by (installed) postfix-2:2.6.6-2.2.el6_1.x86_64
   	mysql-libs is needed by (installed) postfix-2:2.6.6-2.2.el6_1.x86_64
   
   # --nodeps强制卸载
   [root@node1 jdk1.7.0_80]# rpm -e --nodeps mysql-libs-5.1.71-1.el6.x86_64
   [root@node1 jdk1.7.0_80]# rpm -qa | grep mysql
   [root@node1 jdk1.7.0_80]# 
   ```

3. 检查并删除老版本 mysql 的开发头文件和库(如果有的话)

   ```
   [root@node1 ~]# find / -name 'mysql*' 
   如果安装失败，卸载残留文件，从以下目录来删除老版本的mysql
   rm -fr /usr/lib/mysql 
   rm -fr /usr/include/mysql 
   rm -f /etc/my.cnf 
   rm -fr /var/lib/mysql 
   rm -fr /usr/share/mysql
   ```

   注意：卸载后/var/lib/mysql 中的数据及/etc/my.cnf 不会删除，如果确定没用后就手工删除

4. 安装 perl

   ```
   [root@node1 ~]# yum install perl -y
   ```

5. 下载 numactl-2.0.9-2.el6.x86_64.rpm 并安装

   ```
   [root@node1 ~]# cd /opt/apps/rpm_install_mysql/
   [root@node1 rpm_install_mysql]# rpm -ivh numactl-2.0.9-2.el6.x86_64.rpm 
   Preparing...                ########################################### [100%]
      1:numactl                ########################################### [100%]
   ```

6. 安装 mysql（有顺序要求）

   ```
   rpm -ivh mysql-community-common-5.7.19-1.el6.x86_64.rpm 
   rpm -ivh mysql-community-libs-5.7.19-1.el6.x86_64.rpm 
   rpm -ivh mysql-community-client-5.7.19-1.el6.x86_64.rpm 
   rpm -ivh mysql-community-server-5.7.19-1.el6.x86_64.rpm
   ```

   ![image-20211206164646671](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112061646174.png)

   或执行：

   ```
   rpm -ivh mysql-community-*
   ```

   让系统自己判断 rpm 安装顺序

7. 修改`/etc/my.cnf` 文件，设置数据库的编码方式

   ```
   [client] 
   default-character-set=utf8[mysql] 
   default-character-set=utf8
   [mysqld] 
   character_set_server=utf8
   ```

8. 如果出现错误，请查看`/etc/my.cnf` 文件中指定的错误 log 日志的文件

   ```
   log-error=/var/log/mysqld.log
   ```

9. 启动 MySQL：`service mysqld start`

   ```
   [root@node1 rpm_install_mysql]# service mysqld start
   初始化 MySQL 数据库：                                      [确定]
   正在启动 mysqld：                                          [确定]
   ```

10. 找到随机密码：

    ```
    [root@node1 rpm_install_mysql]# vim /var/log/mysqld.log
    
    2021-12-06T08:55:30.750539Z 0 [Warning] TIMESTAMP with implicit DEFAULT value is deprecated. Please use --explicit_defaults_for_timestamp server option (see documentation for more details).
    2021-12-06T08:55:30.983727Z 0 [Warning] InnoDB: New log files created, LSN=45790
    2021-12-06T08:55:31.033087Z 0 [Warning] InnoDB: Creating foreign key constraint system tables.
    2021-12-06T08:55:31.098763Z 0 [Warning] No existing UUID has been found, so we assume that this is the first time that this server has been started. Generating a new UUID: 44221437-5672-11ec-83cb-000c29186dfc.
    2021-12-06T08:55:31.100735Z 0 [Warning] Gtid table is not ready to be used. Table 'mysql.gtid_executed' cannot be opened.
    2021-12-06T08:55:31.101853Z 1 [Note] A temporary password is generated for root@localhost: ttvvo/mJH86Y
    2021-12-06T08:55:34.814181Z 0 [Warning] TIMESTAMP with implicit DEFAULT value is deprecated. Please use --explicit_defaults_for_timestamp server option (see documentation for more details).
    2021-12-06T08:55:34.817600Z 0 [Note] /usr/sbin/mysqld (mysqld 5.7.19) starting as process 8811 ...
    2021-12-06T08:55:34.825251Z 0 [Note] InnoDB: PUNCH HOLE support available
    2021-12-06T08:55:34.825310Z 0 [Note] InnoDB: Mutexes and rw_locks use GCC atomic builtins
    2021-12-06T08:55:34.825317Z 0 [Note] InnoDB: Uses event mutexes
    2021-12-06T08:55:34.825343Z 0 [Note] InnoDB: GCC builtin __sync_synchronize() is used for memory barrier
    2021-12-06T08:55:34.825350Z 0 [Note] InnoDB: Compressed tables use zlib 1.2.3
    2021-12-06T08:55:34.825354Z 0 [Note] InnoDB: Using Linux native AIO
    2021-12-06T08:55:34.825666Z 0 [Note] InnoDB: Number of pools: 1
    2021-12-06T08:55:34.825770Z 0 [Note] InnoDB: Using CPU crc32 instructions
    2021-12-06T08:55:34.827069Z 0 [Note] InnoDB: Initializing buffer pool, total size = 128M, instances = 1, chunk size = 128M
    2021-12-06T08:55:34.844465Z 0 [Note] InnoDB: Completed initialization of buffer pool
    2021-12-06T08:55:34.846674Z 0 [Note] InnoDB: If the mysqld execution user is authorized, page cleaner thread priority can be changed. See the man page of setpriority().
    2021-12-06T08:55:34.857992Z 0 [Note] InnoDB: Highest supported file format is Barracuda.
    2021-12-06T08:55:34.871154Z 0 [Note] InnoDB: Creating shared tablespace for temporary tables
    2021-12-06T08:55:34.871204Z 0 [Note] InnoDB: Setting file './ibtmp1' size to 12 MB. Physically writing the file full; Please wait ...
    2021-12-06T08:55:34.890391Z 0 [Note] InnoDB: File './ibtmp1' size is now 12 MB.
    ```

    在A temporary password is generated for root@localhost，后面就是随机密码：`ttvvo/mJH86Y`

11. 修改默认密码

    ```
    # 使用随机密码登录mysql
    [root@node1 rpm_install_mysql]# mysql -uroot -p"ttvvo/mJH86Y"
    
    # 设置密码的安全级别
    mysql> set global validate_password_policy=0;
    Query OK, 0 rows affected (0.00 sec)
    
    # 设置密码长度
    mysql> set global validate_password_length=6;
    Query OK, 0 rows affected (0.01 sec)
    
    # 设置密码
    mysql> set password for 'root'@'localhost'=password('123456');
    Query OK, 0 rows affected, 1 warning (0.00 sec)
    
    # 修改密码后
    mysql> exit
    Bye
    [root@node1 rpm_install_mysql]# mysql -uroot -p
    Enter password: 
    Welcome to the MySQL monitor.  Commands end with ; or \g.
    ```

12. 登陆 mysql 查看编码方式

    ```
    mysql> show variables like '%character%';
    +--------------------------+----------------------------+
    | Variable_name            | Value                      |
    +--------------------------+----------------------------+
    | character_set_client     | utf8                       |
    | character_set_connection | utf8                       |
    | character_set_database   | utf8                       |
    | character_set_filesystem | binary                     |
    | character_set_results    | utf8                       |
    | character_set_server     | utf8                       |
    | character_set_system     | utf8                       |
    | character_sets_dir       | /usr/share/mysql/charsets/ |
    +--------------------------+----------------------------+
    8 rows in set (0.00 sec)
    ```

13. 给 root 设置远程登录权限

    ```
    mysql> GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY '123456' WITH GRANT OPTION;
    Query OK, 0 rows affected, 1 warning (0.00 sec)
    
    mysql> FLUSH PRIVILEGES;
    Query OK, 0 rows affected (0.00 sec)
    ```

14. 设置 msyql 开机启动

    ```
    [root@node1 rpm_install_mysql]# chkconfig mysqld on
    ```

15. 安装完毕！！！

#### 5.2.4  rpm卸载

&emsp;&emsp;基本命令格式为：`rpm -e 安装包`。实例：找出与 apr 有关的软件名称,并尝试移除 apr 这个软件

```
[root@node1 ~]# rpm -qa|grep apr 
apr-1.3.9-5.el6_9.1.x86_64 
[root@node1 ~]# rpm -e apr-1.3.9-5.el6_9.1.x86_64 
[root@node1 ~]# rpm -qa|grep apr 
[root@node1 ~]#
```

###  5.3 yum安装

&emsp;&emsp;yum安装的好处是如果一个软件包需要其他软件包的支持，则在安装这个包的同时，自动安装相依赖的包，非常方便！

&emsp;&emsp;yum安装的原理是通过 yum 命令从各种源（yum服务器）上获取已经编译好的软件包。可以在`/etc/yum.repos.d/*.repo`文件中查看已经配置的源。

&emsp;&emsp;RHEL/CentOS 系统有许多第三方源，比较流行的比如 RpmForge，RpmFusion，EPEL，Remi 等等。然而需要引起注意的是，如果系统添加了多个第三方源，可能会因此产生冲突 。一个软件包可以从多个源获取，一些源会替换系统的基础软件包，从而可能会产生意想不到的错误。已知的就有 Rpmforge 与 EPEL 会产生冲突。对于这些问题我们建议，调整源的优先权或者有选择性的安装源，但是这需要复杂的操作，如果你不确定如何操作，我们推荐你只安装一个第三方源。

**常用的yum命令**：

1. 查看yum源

   ```
   [root@node1 ~]# yum repolist
   Loaded plugins: fastestmirror
   Loading mirror speeds from cached hostfile
    * base: mirrors.aliyun.com
    * epel: mirrors.aliyun.com
    * extras: mirrors.aliyun.com
    * updates: mirrors.aliyun.com
   file:///mnt/cdrom/repodata/repomd.xml: [Errno 14] Could not open/read file:///mnt/cdrom/repodata/repomd.xml
   Trying other mirror.
   repo id                    repo name                                                          status
   base                       CentOS-6 - Base - mirrors.aliyun.com                                6,713
   epel                       Extra Packages for Enterprise Linux 6 - x86_64                     12,581
   extras                     CentOS-6 - Extras - mirrors.aliyun.com                                 47
   local                      local repo                                                          6,696
   updates                    CentOS-6 - Updates - mirrors.aliyun.com                             1,193
   repolist: 27,230
   ```

2. 删除缓存

   ```
   yum clean all
   ```

3. 生成缓存

   ```
   yum makecache
   ```

4. 升级某rpm包

   ```
   yum update rpm包
   ```

5. 列出系统中已经安装的和可以安装的包

   ```
   yum list
   ```

   ![image-20211206191528556](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112061915726.png)

6. 打印指定包的描述信息

   ```
   yum info  指定包
   ```

![image-20211206191741492](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/202112061917862.png)

7. 安装和卸载

   ```
   yum install xxx 
   yum remove|erase xxx
   ```

8. yum分组命令

   - 查询 yum 源中 rpm 包的组信息

     ```
     yum grouplist
     ```

   - 查看指定组的信息

     ```
     yum groupinfo "Chinese Support"
     ```

   - 安装指定软件组

     ```
     yum groupinstall "Chinese Support"
     ```

   - 删除指定软件组

     ```
     yum groupremove "Chinese Support"
     ```

   - 更新指定软件组

     ```
     yum groupupdate 指定软件组
     ```

     

### 5.4 三种安装方式选择

&emsp;&emsp;对于类似于nginx这种，没有提供rpm包，也不能通过yum进行在线安装，就只能使用源码编译安装；对于类似于jdk这种，需要做相关配置，最好不要使用yum安装，要使用rpm安装；对于一些不需要做配置的安装，直接使用yum安装。

### 5.5 CentOS使用其它源(扩展)

&emsp;&emsp;如果在工作中碰到，使用的源没有所需要的的包，此时，就需要使用其他源。

- 使用清华源

  使用https://mirrors.tuna.tsinghua.edu.cn/help/centos/下对应CentOS的内容来操作

- 使用阿里云

  - 备份

    ```
    mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup
    ```

  - 下载新的 CentOS-Base.repo 到/etc/yum.repos.d/

    ```
    wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-6.repo
    ```

    or

    ```
    curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-6.repo
    ```

  - 清空缓存

    ```
    yum clear all
    ```

  - 生成缓存

    ```
    yum makecache
    ```

    

​		
