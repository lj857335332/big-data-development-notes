[TOC]



# Linux常用命令

## 1.基本命令

### 1.1 命令入门

#### 1.1.1 命令提示符详解

![image-20211122202253587](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122202253587.png)

​		root表示用户名；node1表示主机名；~表示当前目录；#表示系统权限；$表示普通权限

#### 1.1.2 命令格式

​		命令 选项 参数 (三者之间要有空格，区分大小写) ，`command [-options] [args]` 如果`[args]`参数有多个，需要以空格间隔	

<img src="https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122203635906.png" alt="image-20211122203635906" style="zoom:80%;" />

​		上图中，`ls -a /home`表示查看home目录下的文件；`ls -a`表示查看当前目录下的文件；`ls -a -l`表示以列表的形式来显示文件		的相关信息；`ls -al`或者`ls -la`是它的缩写；`ls -al install*`表示查找以install开头的文件

### 1.2 获取命令的帮助

​		当linux操作系统作为服务端，我们希望它的性能更好，所以，很多服务器端的linux系统不会安装图形界面， 只有命令行界面，我们很多操作都在命令行下输入。在学习linux命令时，我们可以通过查手册&搜索引擎来学习，也可以通过man命令与在命令后输入`--help`来学习。

#### 1.2.1 查手册&搜索引擎

​		手册见《参考手册&资料》，其中《鸟哥的linux私房菜：基础学习篇 （第四版）》可以作为工具书来补充学习。

#### 1.2.2 man命令

##### 1.2.2.1 man的安装

​		当前虚拟机中不能直接使用 man，默认是没有安装的，不识别 man 命令

![image-20211122205853085](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122205853085.png)

​		所以需要安装 man 和 man-pages 手册，查看命令的帮助信息

![image-20211122210050530](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122210050530.png)

​		查看命令`ls`

![image-20211122210322966](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122210322966.png)

​		输入:q，退出命令查看

##### 1.2.2.2 汉化man提示信息

​		要想汉化 man 提示信息，系统中需要安装 man-pages-zh-CN 软件包。默认情况下， 系统自带的 yum 源不包含 `man-pages-zh-CN`，所以需要将下载后的第三方源`epel`保存到默认的源目录`/etc/yum.repos.d/`。

1. 安装 wget		![image-20211122212651787](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122212651787.png)

2. 将 epel 的源放到本地，然后清除 yum 本地缓存，重新生成缓存	![image-20211122214909337](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122214909337.png)

3. 安装 man-pages-zh-CN 软件包![image-20211122215122939](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122215122939.png)

4. 修改系统的语言环境为中文（临时修改）

   ```
   [root@node1 ~]# echo $LANG en_US.UTF-8
   en_US.UTF-8
   [root@node1 ~]# LANG=zh_CN.UTF-8 
   [root@node1 ~]# echo $LANG 
   zh_CN.UTF-8
   ```

   长期修改：`[root@node1 ~]# vim /etc/sysconfig/i18n`

   ![image-20211122215711470](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122215711470.png)

   保存退出，重启后就可以使用了

   ![image-20211122220256774](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211122220256774.png)

##### 1.2.2.3 man的使用

​	**enter 向下一行**

​	**空格按页向下翻** 

​	**b** **向上翻页** 

​	**p** **直接翻到首页** 

​	**查找按** **/**要查找的内容，查找 下一个/上一个：按**n/N**；

​	**退出按** **q** 

#### 1.2.3 命令 --help

​		通过`命令 --help`可以查看命令中参数的作用

![image-20211124212143255](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211124212143255.png)

### 1.3 基本命令echo、ls、pwd、cd

#### 1.3.1 基本命令echo

​		echo用于显示一行文本，

​		![image-20211125135325777](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125135325777.png)

​		`echo -n` 表示不要另起新行

![image-20211125135519577](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125135519577.png)

​		`echo -e` 表示解释逃逸字符 

![image-20211125135659325](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125135659325.png)

​		`echo`也可用于显示环境变量与自定义变量

![image-20211125135906773](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125135906773.png)

​		当在 shell 中执行命令的时候，默认到 PATH 指定的路径中查找可执行文件。 如果在 PATH 中的多个目录都包含该可执行文件，则执		行最先找到的。为什么ls、mv这些命令可以直接用呢？因为都在 PATH 指定的路径的可执行文件中

![image-20211125140334308](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125140334308.png)

#### 1.3.2 基本命令ls

​		ls 命令用于列出目录内容 ，单独的`ls`命令用于查看当前目录下的内容，隐藏文件并不会显示

![image-20211125140957120](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125140957120.png)

​		`ls -l`:添加-l 以长格式输出，列出根目录下内容的详细列表

![image-20211125141015550](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125141015550.png)

​		ls命令可以后面跟多个目录，列出多个目录的内容，目录间以空格分隔

![image-20211125141203150](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125141203150.png)

​		`ls -a`显示所有的内容，包括隐藏文件；`ls -al`以列表的方式显示所有的内容

![image-20211125141527774](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125141527774.png)

​		`ls -R`：可以显示多级目录

![image-20211125144141810](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125144141810.png)

#### 1.3.3 基本命令pwd

​		`pwd`查看当前工作目录路径

#### 1.3.4 基本命令cd

​		`cd`命令可以用于切换目录

​		如果 cd 后什么都不写或者cd 后跟波浪线，表示直接回当前用户家目录

![image-20211125142114890](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125142114890.png)

​		root 用户可以直接通过绝对路径进到普通用户的家目录；也可以直接跟波浪线用户名表示直接进入到某个用户的家目录

![image-20211125142455524](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125142455524.png)

​		cd 后也可以跟减号（-）用以表示回到最后一次切换之前的目录，多次使用减号在最近两个目录之间切换 

![image-20211125142731979](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125142731979.png)

​		`cd ..`表示用于返回上一级目录

![image-20211125143022430](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125143022430.png)

### 1.4 基本命令mkdir、rm

#### 1.4.1 基本命令mkdir

​		`mkdir` ：用于创建目录

![image-20211125143618434](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125143618434.png)

​		`mkdir -p`：用以创建多层目录，因为系统发现某一级目录不存在的时候创建父目录

![image-20211125143921013](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125143921013.png)

​		可以使用大括号高效创建相似的目录

![image-20211125144742788](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125144742788.png)

#### 1.4.2 基本命令rm

​		`rm`命令用于删除文件；直接删除，需要确认 yes

![image-20211125145352714](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125145352714.png)

​		`rm -f`：添加-f 选项可以不用确认强制删除

![image-20211125145535012](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125145535012.png)

​		`rm -r`（删除文件夹）：如果 rm 的参数是目录，则会提示需要迭代删除而不能成功，添加-r 参数表示迭代删除；

![image-20211125145949052](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125145949052.png)

​		`rm -rf`可以使用-rf 选项，迭代强制删除某个文件或目录，此命令慎用

![image-20211125150042879](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125150042879.png)

### 1.5 基本命令cp、mv、ln

#### 1.5.1 基本命令cp

　　`cp 文件… 目标目录` 表示拷贝文件到目标目录，可以同时拷贝多个文件，文件名之间用空格隔开 

![image-20211125171715138](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125171715138.png)

​		`cp -r 目录 目标目录`表示拷贝目录到目标目录，`-r`表示迭代的拷贝某个目录

![image-20211125172040605](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125172040605.png)

#### 1.5.2 基本命令mv

​		`mv` 用于移动或重命名文件，它与cp、rm命令不同，可以移动文件/目录，不需要`-r`

![image-20211125172428092](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125172428092.png)

​		Linux 中没有专门改名的命令，`mv`命令兼职改名，可以更改文件与文件夹的名字

![image-20211125190747434](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125190747434.png)

#### 1.5.3 基本命令ln

　　`ln`用于创建硬链接或软链接 

​		`ln`创建硬链接，创建的是一个文件，大小与源文件相同

![image-20211125191630207](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125191630207.png)

​		`ln -s`用于创建软链接，当编辑ln2时，实际上打开的是profile

![image-20211125191255257](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125191255257.png)

​		使用 `ll -i` 选项让 ll 打印文件的 inode 信息，发现硬链接创建的文件与源文件是同一个文件。如果删除源文件，则软链接就找不到		文件，而硬链接可以找到

![image-20211125192947849](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125192947849.png)

​		当将文件重新复制一份，发现软链接可以找到新文件，硬链接与新文件不一致

![image-20211125194053896](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125194053896.png)

### 1.6 进程状态查看命令ps

​		`ps`命令查看系统内进程信息 

​		`ps -ef`查看磁盘使用情况

![image-20211125194550965](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125194550965.png)

​		`ps aux` 观察系统所有的程序数据 （常用）,其中，

​				1. a :和输入终端（terminal） 关联的所有 process，通常与 x 一起使用， 列出完整信息。 

​				2. x :后台进程，通常与 a 这个参数一起使用,可列出较完整信息 

​				3. u :有效使用者 (effective user) 相关的 process

![image-20211125200939409](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125200939409.png)

​		过滤查看与pts/1有关的进程

![image-20211125201058751](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125201058751.png)

​		**以下为各选项的含义** 

​			 USER:该 process属于那个使用者 

​			 PID :该 process 的程序标识符。

​			 %CPU:该 process 使用掉的 CPU资源百分比 

​			 %MEM:该process 所占用的物理内存百分比

​			 VSZ :该 process 使用掉的虚拟内存量 (Kbytes)

​			 RSS :该 process占用的物理的内存量 (Kbytes) 

​			 TTY :该 process 是在那个终端机上面运作，若与终端机无关则显示 ?另外，tty1-tty6是本机上面的登入者程序,若为 pts/0 等等				,则表示为由网络连接进主机的程序。

​			 STAT:该进程目前的状态,状态显示与 ps -l 的 S旗标相同 (R/S/D/T/Z)

​			 START:该 process被触发启动的时间 

​			 TIME :该 process实际使用 CPU 运作的时间。

### 1.7 test命令

​		计算 3 > 2 的结果，打印返回值，计算 3 < 2 的结果，打印返回值。 `echo $?` 打印上一行命令的执行结果，成立返回 0（

​		true），不成立返回 1（false）。其中，`-gt`是大于，`-ge`是大于等于，`-lt`是小于，`-le`是小于等于，`-eq`是等于

![image-20211125203034576](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125203034576.png)

​		上面的命令也可以用如下方式来表示：

![image-20211125204502339](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125204502339.png)

​		`[]`和内容之间一定要有空格，否则报错。

## 2. 文件系统命令

### 2.1 虚拟目录

　　虚拟目录中各个目录的用途

| 目录   | 用途                                           |
| ------ | ---------------------------------------------- |
| /      | 虚拟目录的根目录。通常不会在这里存储文件       |
| /bin   | 二进制目录，存放许多用户级的 GNU 工具          |
| /boot  | 启动目录，存放启动文件                         |
| /dev   | 设备目录，Linux 在这里创建设备节点             |
| /etc   | 系统配置文件目录                               |
| /home  | 主目录，Linux 在这里创建用户目录               |
| /lib   | 库目录，存放系统和应用程序的库文件             |
| /media | 媒体目录，可移动媒体设备的常用挂载点           |
| /mnt   | 挂载目录，另一个可移动媒体设备的常用挂载点     |
| /opt   | 可选目录，常用于存放第三方软件包和数据文件     |
| /proc  | 进程目录，存放现有硬件及当前继承的相关信息     |
| /root  | root 用户的主目录                              |
| /sbin  | 系统二进制目录，存放许多 GNU 管理员级工具      |
| /srv   | 服务目录，存放本地服务的相关文件               |
| /sys   | 系统目录，存放系统硬件信息的相关文件           |
| /tmp   | 临时目录，可以在该目录中创建和删除临时工作文件 |
| /usr   | 大量用户级的 GNU 工具和数据文件都存储在这里    |
| /var   | 可变目录，用以存放经常变化的文件，比如日志文件 |

### 2.2 文件系统命令df、mount、umount

#### 2.2.1 df命令

​		`df`查看系统挂载的磁盘情况

![image-20211125214450718](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125214450718.png)

​		`df -h`以适合的单位查看系统挂载的磁盘情况

![image-20211125214540623](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125214540623.png)

#### 2.2.2 mount命令与umount命令

​		将光驱挂载到/mnt 目录与卸载掉挂载的分区/mnt

![image-20211125220004599](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125220004599.png)

## 3.系统操作命令

### 3.1 du命令

​		`du`命令可以为目录递归地汇总每个 FILE 的磁盘使用情况

​				ￚ a :列出所有的文件与目录容量

​				ￚ h :以人们较易读的容量格式(G/M)显示 

​				ￚ s :列出总量,而不列出每个各别的目录占用容量 

​				ￚ k :以 KBytes 列出容量显示

​				ￚ m :以MBytes列出容量显示

​		`du -s`：添加-s 参数可以生成指定目录的汇总信息，也就是共占用多大的磁盘空间

![image-20211125220736128](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125220736128.png)

​		`du -sh`：添加-h 参数可以显示为人类可以读懂的格式

![image-20211125220848891](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125220848891.png)

​		将路径写成./*统计当前目录下每项内容占用的磁盘空间信息

![image-20211125220937087](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125220937087.png)

### 3.2 stat命令

​		`stat`命令：显示文件的元数据

![image-20211125221600302](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125221600302.png)

### 3.3 touch命令

​		touch 已存在的文件，抹平各个时间 ；touch 不存在的文件，则创建文件

![image-20211125221859058](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211125221859058.png)

## 4.文本操作命令

### 4.1 cat命令

​		`cat`:直接查看一个文件的内容，适合查看行数相对较少的文件（100行左右）；nl与tac指令与cat类似

​		![image-20211126140901045](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211126140901045.png)

​		`nl`命令：添加行号打印

​		![image-20211126140918229](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211126140918229.png)

![image-20211126140951779](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211126140951779.png)

​		`tac`命令：反向输出，tac 功能与 cat 类似,但是是由文件最后一行反向连续输出到屏幕上

![image-20211126141128669](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211126141128669.png)

### 4.2 head命令

​		`head`命令表示显示前几行，默认显示前10行，可使用`head -n`来控制显示前n行

![image-20211126142207725](../../../../AppData/Roaming/Typora/typora-user-images/image-20211126142207725.png)

​		`head -n -120`表示后面120行都不打印

![image-20211126142619420](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211126142619420.png)

### 4.3 tail命令

​		命令格式：`tail [ -n number] 文件` 

​		选项与参数： `-n`:后面接数字，代表显示几行的意思 ；`-f`:表示持续侦测后面文件内容的改变，直到按下 Ctrl+c 才会结束 tail 的侦		测。 

​		`tail`命令：默认情况下显示最后 10 行

![image-20211126142934605](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211126142934605.png)

​		`tail -n 20 文件`默认显示后面的20行

![image-20211126143123455](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211126143123455.png)

​		`tail -n +50 文件`显示从第50行开始直至文件结尾部分的内容

![image-20211126143503889](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211126143503889.png)

​		`tail -f 文件`表示持续侦测后面文件内容的改变，直到按下 Ctrl+c 才会结束 tail 的侦测。 

### 4.4 管道|

​		`|`:管道左侧的输出作为右侧的输入,常与`grep`一起使用用于过滤

​		`ps aux|grep bash`:过滤查看与bash有关的进程

![image-20211126165754195](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211126165754195.png)

​		`head -n 20 profile | tail -n 10`:查看文件的中间10行

![image-20211126170645752](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211126170645752.png)

### 4.5 xargs

​		`xargs`:将前面输出作为后面命令的参数，将步骤 1 的内容做为步骤 2 的命令的选项参数拼接起来,执行得到的结果

![image-20211126171424822](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211126171424822.png)

### 4.6 数据重定向

​		后期，在写shell脚本与操作时，我们会将数据重定向到别的位置

​		标准输入（stdin）:编号为 0 

​		标准输出（stdout）:编号为 1 

​		标准错误输出（stderr）:编号为 2 

​		1>:以覆盖的方法,将正确的数据输出到文件; 

​		1>>:以追加的方法,将正确的数据输出到文件; 

​		2>:以覆盖的方法,将错误输出的数据输出到文件; 

​		2>>:以追加的方法,将错误输出的数据输出到文件;

​	![image-20211126174309045](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211126174309045.png)

​		![image-20211126174329503](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211126174329503.png)

​		tee 命令，将输入分成两个输出 ,既向控制台输出，也向文件写入

​		![image-20211126174348364](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211126174348364.png)

### 4.7 cut 命令

​		`cut`命令：显示切割的行数据 ，源文件并不会改变

				- `-s`：不显示没有分隔符的行
				- `-d`：指定分隔符对源文件的行进行分割 

- `-f` :选定显示哪些列 ,其中`m-n`表示 m 列到 n 列 ；`-n`表示 第一列到 n 列 ；`m-`表示 第 m 列到最后一列；`n`表示 第 n 列 ；`x,y,z`表示 获取第 x,y,z 列

![image-20211126194819362](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211126194819362.png)

![image-20211126195126346](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211126195126346.png)

![image-20211126195507076](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211126195507076.png)

​		`-- output-delimiter`表示指定输出的时候的各字符分隔符

![image-20211126200010876](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211126200010876.png)

### 4.8 sort命令

​		`sort`：排序文件的行 

- -n：按数值排序 

- -r：倒序 reverse 

- -t：自定义分隔符 

- -k：选择排序列 

- \- f：忽略大小写

​		以如下sort.txt文件为例进行排序

```
a b 1
dfdsa fdsa 15
fds fds 6
fdsa fdsa 8
fda s 9
aa dd 10
h h 11
```

​		`sort`默认按字符串字典序排序（升序），并不会改变源文件的内容

![image-20211126201052296](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211126201052296.png)

​		`sort -t ' ' -k 2 文件`：表示指定字段分隔符，按照第 2 个字段的字典序排序

![image-20211126201444692](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211126201444692.png)

​		`sort -t ' ' -k 3 -n 文件`：表示指定字段分隔符，按照第 3 个字段的值数值序排序 ，如果不添加`-n`会将数字当成字符串排序

![image-20211126201855182](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211126201855182.png)

​		`-r`：添加-r倒序排序

![image-20211126202137815](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211126202137815.png)

### 4.9 sed命令

​		`sed`又称为行编辑器，可以将文件里的内容逐行进行读取逐行进行编辑。使用场景是有5000台服务集群，每个集群上都有配置文件，想要快速修改配置文件，此时，就需要`sed`文件，不需要人为的打开文件，而是通过脚本定制化的修改它

命令格式：`sed [选项] 'AddressCommand' file`

- `-i`：直接修改源文件 
- `-r`：表示使用扩展正则表达式 

- `-d`：删除符合条件的行 

- `-a\string`：在指定的行后追加新行，内容为 string 

- `i\string`：在指定行前添加新行，内容是 string 

- `s/string1/string2/`：查找并替换，默认只替换每行第一次模式匹配到的字符串

- `g`：行内全局替换 

- `i`：忽略大小写 

- `\(\) \1\2`

​		以新建的sed.txt文件为例，

```
Authentication improvements when using an HTTP proxy server.
Support for POSIX-style filesystem extended attributes.
YARN's REST APIs now support write/modify operations.
```

![image-20211126211331073](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211126211331073.png)

- `a\string`：在指定的行后追加新行，内容为 string，a前面的1表示在第一行后追加string，并且未改变源文件

![image-20211126211853456](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211126211853456.png)

- `i\string`：在指定行前添加新行，内容是 string 

![image-20211126214148587](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211126214148587.png)

- `-i`:直接修改源文件 ，其中 \n可用于换行

![image-20211126212234567](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211126212234567.png)

- `d`：删除符合条件的行（默认删除所有行） ,源文件未改变，可以与`-i`一起使用

![image-20211126214533190](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211126214533190.png)

- 查找指定行`/str/`，`-n`表示只显示符合条件的

![image-20211126215603930](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211126215603930.png)

- `s/string1/string2/`：查找并替换，默认只替换每行第一次模式匹配到的字符串

![image-20211126215937192](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211126215937192.png)

- `i`：忽略大小写

​		`sed "s/filesystem/FS/i" sed.txt`

- `g`:行内全局替换

​		`sed "s/filesystem/FS/g" sed.txt`

- `\(\) \1\2`：下面的命令格式为：`s/\(\)[]\(\)/\1\2/`,`\1`与`\2`之间为要替换的数字

​		以将启动文件中的默认运行级别改为 5

![image-20211126221504731](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211126221504731.png)

![image-20211126221531637](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/image-20211126221531637.png)

- `-r`：表示使用扩展正则表达式，这是就可以简化上面表达式	

​		`sed -r "s/(id:)[0-6](:initdefault:)/\15\2/" inittab`

- `-n`:表示抑制自动打印，如果不添加`-n`会将文件自动打印出来

​		查找/etc/profile 中包含 PATH 的行，将这些行写到profile_path.txt文件中

![1637994681418](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/1637994681418.png)

### 4.10 awk

​		awk 是一个强大的文本分析工具，相对于 grep 查找，sed 编辑，awk 在对数据分析并生成报告时更为强大，awk 把文件逐行读入，以空格和制表符作为默认分隔符将每行切片，切开的部分再进行各种分析处理。

命令格式为：`awk -F [':'] '{pattern + action}' filename`

- 支持自定义分隔符 

- 支持正则表达式匹配 

- 支持自定义变量，数组 a[1] a[tom] map(key) 

- 支持内置变量 

   FS :设置输入列分隔符，等价于命令行 -F 选项 

   NF :浏览记录的列的个数 

   NR :已读的行记录数 

   OFS:输出列分隔符 

   ORS :输出行记录分隔符 

   RS :控制记录分隔符 

- 支持函数 :print、split、substr、sub、gsub 

- 支持流程控制语句，类 C 语言 :if、while、do/while、for、break、continue 

  

　　下面举几个例子学习上述命令

​		１.　搜索/etc/passwd 有 root 关键字的所有行

![1637995646339](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/1637995646339.png)

​		２.　统计/etc/passwd 文件中，每行的行号，每行的列数，对应的完整行内容

![1638001295051](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/1638001295051.png)

​		３.　只是显示/etc/passwd 的账户，即第一列

![1638001387855](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/1638001387855.png)

​		４.　显示/etc/passwd 的账户和账户对应的 shell，即第一列与第七列；而账户与 shell 之间以制表符分割

![1638001594528](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/1638001594528.png)

​		５.　在所有行开始前添加列名 name,shell,在最后一行添加"shell,end"

![](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/1638002167706.png)

### 4.11 通过awk实现报表统计

​		案例报表统计：合计每人 1 月总消费，0：manager，1：worker

```
Tom 0 2020-10-11 car 3000 
John 1 2020-01-13 bike 1000 
vivi 1 2020-01-18 car 2800 
Tom 0 2020-01-20 car 2500 
John 1 2020-01-28 bike 3500
```

​		新建emp.txt文件，将上述内容写入文件中，

1.查看文件的分隔符，awk默认制表符与分隔符
2.日期是第三列，由于统计到的是一月份，所以需要拆分，以"-"进行拆分，并存放到date变量中
3.for循环进行输出，写法与C语言的写法一致

```
awk '
{ 
	split($3, date, "-") 
	if (date[2] == "01"){
	 map_name_sala[$1] += $5 
	if($2=="0"){ 
	 map_name_role[$1]="Manager" 
	}else{ 
	 map_name_role[$1]="Worker" 
	} 
	}
}
END{
	for(name in map_name_sala){
	 print name"\t"map_name_role[name]"\t" map_name_sala[name]
	} 
}' emp.txt
```

![1638013802283](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/1638013802283.png)

### 4.12 wc命令

​		命令格式：`wc [选项列表]... [文件名列表]`，用于输出文件中的行数、单词书、字节数

描述 ：对每个文件输出行、单词、和字节统计数，如果指定了多于一个文件则还有一个行数的总计。没有指定文件或指定的文件是 -，则读取标准输入。 

- -c, --bytes, --chars 输出字节统计数。 
- -l, --lines 输出换行符统计数。
- -L, --max-line-length 输出最长的行的长度。 
- -w, --words 输出单词统计数。 
- --help 显示帮助并退出 
- --version 输出版本信息并退出 		

### 4.13 vi/vim编辑器的使用

​		vi、vim的使用基本一致，推荐使用vim；vim会对于不同的字使用不同的颜色。vim有三种模式：

​		![1638014319156](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/1638014319156.png)

```
i：进入编辑模式 
I：在当前行首（非空字符）进入编辑模式
a：在选定字符后插入字符 
A：在当前行末进入编辑模式
o：在当前行下添加新行 
O：在当前行上添加新行
ESC：退出编辑模式
shift+: 末行模式  ESC 退出末行模式 
shift+ZZ：在命令模式保存并退出编辑器 
:wq 保存并退出编辑器 
:w 保存编辑器内容 
:q! 不保存退出编辑器
```

​		vim的使用技巧：

```
移动光标：h 左 j 下 k 上 l 右 
w：移动到下一个单词的词首 
e：跳至当前或下一个单词的词尾 
b：跳至当前或下一个单词的词首 
0：绝对行首 
^：行首的第一个非空白字符 
$：绝对行尾 
G：文档末尾 
3G：第三行 
gg：文档开头 
ctrl-f：向下翻页 forward 
ctrl-b：向上翻页 backward 

删除替换单个字符 
x：删除光标位置字符 
3x：删除光标开始3个字符 
r：替换光标位置字符
dw：删除单词 
dd：删除整行
D:删除光标所在位置到行尾 
yw：复制单词 
yy：复制1行 
nyy：复制n行，n是数字 
p：粘贴 paste 
u：撤销 undo 
ctrl+r：重做 操作结束后使用u退回到上次操作，则ctrl+r重做。重复上一步操作

set：设置 ，在末行模式下
:set nu  显示行号 
:set nonu  取消行号的显示 
:set readonly 设置只读 
:/string  or :?string 查找string
    n:向下查找
    N：向上查找 
:! 执行命令，可以不用退出保存就可以执行命令，再按ENTER返回vim模式

查找并替换 
:s/str1/str2/gi 
    g：一行内全部替换 
    i：忽略大小写 
    n：行号 
可以在s前面添加行数如：:12,17s/PATH/path/gi，也可以使用当前光标行与偏移操作如：:.,+3s/PATH/path/gi
    .:当前光标行 
    +n:偏移n行 
$：末尾行，$-3最后三行 
%：全文 
:%d ：删除全文 
:.,$-1d ：从当前行删除到倒数第二行 
:.,+3d ：从当前行再往下数三行删除 
:.,13d ：从当前行到第13行删除
```

## 5.文件压缩与打包

​		**压缩**：指通过某些算法，将文件尺寸进行相应的缩小，同时不损失文件的内容。 

​		**打包**：指将多个文件（或目录）合并成一个文件，方便传递或部署。 

压缩文件或打包文件常见的扩展名： ***.tar.gz, \*.tar.bz2**；linux 系统一般文件的扩展名用途不大，但是压缩或打包文件的扩展名是必须的，因为 linux 支持的压缩命令较多，不同的压缩技术使用的压缩算法区别较大，根据扩展名能够使用对应的解压算法。

常见文件扩展名：

- *.tar.gz　tar 程序打包的文件,并且经过 gzip 的压缩 
- *.tar.bz2　tar 程序打包的文件,并且经过 bzip2 的压缩 

gzip压缩算法与bzip2压缩算法相比，压缩速度更快，但是没有bzip2压缩算法压缩的小

**TAR 命令，选项与参数：**

- -c :建立打包文件, 
- -t :查看打包文件的内容含有哪些文件 
- -x :解打包或解压缩的功能,可以搭配-C(大写)在特定到特定目录解开

- -j :通过 bzip2 的支持进行压缩/解压缩:此时文件最好为 *.tar.bz2 

- -z :通过 gzip 的支持进行压缩/解压缩:此时文件最好为 *.tar.gz

- -v :在压缩/解压缩的过程中,将正在处理的文件名显示出来 

- -f filename:-f 后面跟处理后文件的全名称（路径+文件名+后缀名） 

- -C 目录:这个选项用在解压缩,若要在特定目录解压缩,可以使用这个选项 

- -p :保留备份数据的原本权限与属性,常用于备份(-c)重要的配置文件 

  **注意** -c, -t, -x 不可同时出现在一串指令列中 ，-j，-z不可能同时出现在同一串指令中

**TAR常用的指令组合：**

- 打包与压缩：

  ```
  tar –zcv –f [/路径/]filename.tar.gz 被压缩的文件或目录 
  tar –jcv –f [/路径/] filename.tar.bz2 被压缩的文件或目录
  ```

![1638020369830](https://gitee.com/image_bed1/drawing-bed-1/raw/master/drawing-bed-1/1638020369830.png)

- 查询

  ```
  tar –ztv –f [/路径/] filename.tar.gz 
  tar –jtv –f [/路径/] filename.tar.bz2
  ```

- 备份

  ```
  tar –zpcv –f [/路径/]filename.tar.gz 被备份文件或目录 
  tar –jpcv –f [/路径/]filename.tar.bz2 被备份文件或目录
  ```

- 解压到当前目录

  ```
  tar –jxv –f [/路径/] filename.tar.bz2 
  tar –zxv –f [/路径/] filename.tar.gz
  ```

- 解压到指定目录

  ```
  tar -jxv -f [/路径/] filename.tar.bz2 –C 指定目录 
  tar -zxv -f [/路径/] filename.tar.gz -C 指定目录
  ```

- 将/etc 压缩到/tmp/下 etc01.tar.gz

  ```
  [root@node1 ~]# tar -zcvf /tmp/etc01.tar.gz /etc
  ```

- 将/tmp/下 etc01.tar.gz 解压到/tmp/目录下

  ```
  [root@tedu ~]# cd /tmp/ #首先进入对应目录 
  [root@tedu tmp]# tar -zxvf etc01.tar.gz
  ```

- 将/tmp/下 etc01.tar.gz 解压到/usr/目录下

  ```
  [root@tedu tmp]# tar -zxvf etc01.tar.gz -C /usr 
  ```

