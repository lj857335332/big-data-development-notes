## 大数据开发笔记

- linux操作系统
  - linux操作系统概述与安装
  - linux常用命令
  - linux用户权限和软件安装与管理
  - shellscript脚本编程
- 大型网站高并发处理
  - Nginx的高并发处理
  - Nginx和keepalived高可用

- zookeeper分布式协调服务框架
  - ZooKeeper初探
  - ZooKeeper分布式集群安装
  - ZooKeeper底层原理剖析与命令实战
  - ZooKeeperAPI实战
  - 分布式RMI协调实战

